/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securityfilter.example;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import eu.chorevolution.securityfilter.api.ProtectionAuthNInterface;

/**
 * Authentication ope.ation to validate the authentication realized to protect the choreography
 */
public class ProtectionAuthNExample implements ProtectionAuthNInterface{

	/**
	 * Method to be implemented to validate the authentication
	 * @param request the request received by the Security Filter
	 * @return the result of the Authentication (true of false)
	 * 
	 */
	public boolean checkProtectionAuthN(ServletRequest request) {
		boolean resultAuthN = false;
		System.out.println("ProtectionAuthNExample->checkProtectionAuthN ()");
		System.out.println(request.getServerName());

		HttpServletRequest httpRequest = (HttpServletRequest) request;
		Enumeration<String> headerNames = httpRequest.getHeaderNames();

		if (headerNames != null) {
			while (headerNames.hasMoreElements()) {
				System.out.println("Header: " + httpRequest.getHeader(headerNames.nextElement()));
			}
		}
		
		
		String gRecaptchaResponse = httpRequest
				.getHeader("g-recaptcha-response");
		
		System.out.println(gRecaptchaResponse);
		try {
			resultAuthN = VerifyRecaptcha.verify(gRecaptchaResponse);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return resultAuthN;

	}
}
