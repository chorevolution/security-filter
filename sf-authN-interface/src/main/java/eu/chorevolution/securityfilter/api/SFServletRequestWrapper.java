package eu.chorevolution.securityfilter.api;
import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import org.apache.commons.io.IOUtils;

public class SFServletRequestWrapper extends HttpServletRequestWrapper{

	private Map<String, String> headerMap;
	private Map<String, String[]> parameterMap;
	private HttpServletRequest wrapped;
	private byte[] rawData;
	private ResettableServletInputStream servletStream;
	private int lenght = 0;


	public void resetInputStream(byte[] newRawData) {
		System.out.println("reset Input");
		System.out.println(new String(newRawData));
		System.out.println("length : " +  newRawData.length);
		this.lenght = newRawData.length;
		servletStream.stream = new ByteArrayInputStream(newRawData);
	}

	@Override
	public ServletInputStream getInputStream() throws IOException {
		if (rawData == null) {
			rawData = IOUtils.toByteArray(this.wrapped.getReader());
			servletStream.stream = new ByteArrayInputStream(rawData);
		}
		return servletStream;
	}

	@Override
	public BufferedReader getReader() throws IOException {
		if (rawData == null) {
			rawData = IOUtils.toByteArray(this.wrapped.getReader());
			servletStream.stream = new ByteArrayInputStream(rawData);
		}
		return new BufferedReader(new InputStreamReader(servletStream));
	}

	
//	public void setContentLength(int lengthContent){
//		this.lenght = lengthContent;
//	}
	
	@Override
	public int getContentLength() {
		// TODO Auto-generated method stub
		return this.lenght;
		
	} 
	private class ResettableServletInputStream extends ServletInputStream {

		private InputStream stream;

		@Override
		public int read() throws IOException {
			return stream.read();
		}

		@Override
		public boolean isFinished() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean isReady() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void setReadListener(ReadListener arg0) {
			// TODO Auto-generated method stub
			
		}
	}


	public void addHeader(String name, String value){
		headerMap.put(name, new String(value));
	}

	public SFServletRequestWrapper(HttpServletRequest request){
		super(request);
		headerMap = new HashMap<String, String>();
		this.wrapped = request;
		this.servletStream = new ResettableServletInputStream();
	}


	public Enumeration<String> getHeaderNames(){
		HttpServletRequest request = (HttpServletRequest)getRequest();
		List<String> list = new ArrayList<String>();
		for( Enumeration<?> e = request.getHeaderNames() ;  e.hasMoreElements() ; )
			list.add(e.nextElement().toString());
		for( Iterator<String> i = headerMap.keySet().iterator() ; i.hasNext() ; ){
			list.add(i.next());
		}
		return Collections.enumeration(list);
	}
	public String getHeader(String name){
		Object value;
		if((value = headerMap.get(""+name)) != null)
			return value.toString();
		else
			return ((HttpServletRequest)getRequest()).getHeader(name);
	}

	public void addParameter(String name, String value) {
		if (parameterMap == null) {
			parameterMap = new HashMap<String, String[]>();
			parameterMap.putAll(wrapped.getParameterMap());
		}
		String[] values = parameterMap.get(name);
		if (values == null) {
			values = new String[0];
		}
		List<String> list = new ArrayList<String>(values.length + 1);
		list.addAll(Arrays.asList(values));
		list.add(value);
		parameterMap.put(name, list.toArray(new String[0]));
	}

	@Override
	public String getParameter(String name) {
		if (parameterMap == null) {
			return wrapped.getParameter(name);
		}

		String[] strings = parameterMap.get(name);
		if (strings != null) {
			return strings[0];
		}
		return null;
	}

	@Override
	public Map<String, String[]> getParameterMap() {
		if (parameterMap == null) {
			return wrapped.getParameterMap();
		}

		return Collections.unmodifiableMap(parameterMap);
	}

	@Override
	public Enumeration<String> getParameterNames() {
		if (parameterMap == null) {
			return wrapped.getParameterNames();
		}

		return Collections.enumeration(parameterMap.keySet());
	}

	@Override
	public String[] getParameterValues(String name) {
		if (parameterMap == null) {
			return wrapped.getParameterValues(name);
		}
		return parameterMap.get(name);
	}

}