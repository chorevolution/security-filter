<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2015 The CHOReVOLUTION project Licensed under the Apache License, 
	Version 2.0 (the "License"); you may not use this file except in compliance 
	with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 
	Unless required by applicable law or agreed to in writing, software distributed 
	under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES 
	OR CONDITIONS OF ANY KIND, either express or implied. See the License for 
	the specific language governing permissions and limitations under the License. -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<organization>
		<name>The CHOReVOLUTION project</name>
		<url>http://www.chorevolution.eu</url>
	</organization>
	<url>http://www.chorevolution.eu</url>
	<developers>
		<developer>
			<id>frederic_motte</id>
			<name>Frederic Motte</name>
			<email>frederic.motte@thalesgroup.com</email>
			<organization>Thales communications and security</organization>
			<organizationUrl>https://www.thalesgroup.com/</organizationUrl>
			<timezone>France/Paris</timezone>
		</developer>
	</developers>
	<licenses>
		<license>
			<name>The Apache Software License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
			<distribution>repo</distribution>
		</license>
	</licenses>

	<scm>
		<connection>scm:git:ssh://gitolite@tuleap.ow2.org/chorevolution/security-filter.git</connection>
		<developerConnection>scm:git:ssh://gitolite@tuleap.ow2.org/chorevolution/security-filter.git</developerConnection>
		<url>https://tuleap.ow2.org/plugins/git/chorevolution/security-filter</url>
		<tag>HEAD</tag>
	</scm>

	<issueManagement>
		<system>jira</system>
		<url>https://jira.ow2.org/browse/CRV</url>
	</issueManagement>

	<name>Security Filter Protection Authentication Example</name>
	<groupId>eu.chorevolution.securityfilter</groupId>
	<artifactId>sf-protection-authN</artifactId>
	<version>1.0.0</version>
	<!-- <packaging>jar</packaging> -->
	<description>CHOReVOLUTION Security filter protection authentication implementation example</description>
	<distributionManagement>
		<repository>
			<id>ow2-nexus-releases</id>
			<name>OW2 Release Repository</name>
			<url>http://repository.ow2.org/nexus/service/local/staging/deploy/maven2/</url>
		</repository>
		<snapshotRepository>
			<id>ow2-nexus-snapshots</id>
			<name>OW2 Snapshots Repository</name>
			<url>http://repository.ow2.org/nexus/content/repositories/snapshots/</url>
		</snapshotRepository>
	</distributionManagement>




	<properties>
		<java.version>1.8</java.version>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<dependencies>
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>javax.servlet-api</artifactId>
			<version>3.1.0</version>
		</dependency>
		<dependency>
			<groupId>eu.chorevolution.securityfilter</groupId>
			<artifactId>sf-authN-interface</artifactId>
			<version>1.0.0</version>
		</dependency>
	</dependencies>
	<build>
		<plugins>
			<plugin><!-- Plugin pour compilation code Java -->
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.5.1</version>
				<configuration>
					<!-- Java version for compiling the source code -->
					<source>${java.version}</source>
					<target>${java.version}</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-release-plugin</artifactId>
				<version>2.5.3</version>
				<configuration>
					<useReleaseProfile>false</useReleaseProfile>
					<goals>deploy</goals>
					<arguments>-Prelease ${arguments}</arguments>
					<waitBeforeTagging>10</waitBeforeTagging>
					<mavenExecutorId>forked-path</mavenExecutorId>
					<autoVersionSubmodules>true</autoVersionSubmodules>
					<tagNameFormat>sf-protection-authN-@{project.version}</tagNameFormat>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<executions>
					<execution>
						<id>attach-sources</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<executions>
					<execution>
						<id>attach-javadocs</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<!-- Assembly Plugin for the win! -->
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-assembly-plugin</artifactId>
				<version>2.6</version>
				<configuration>
					<descriptors>
						<descriptor>src/main/assembly/default.xml</descriptor>
					</descriptors>
				</configuration>
				<executions>
					<execution>
						<id>assembly</id>
						<phase>package</phase>
						<goals>
							<goal>single</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
	<repositories>
		<repository>
			<id>sonatype</id>
			<url>https://oss.sonatype.org/content/repositories/snapshots</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>ow2-snapshots</id>
			<url>http://repository.ow2.org/nexus/content/repositories/snapshots/</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>ow2-releases</id>
			<url>http://repository.ow2.org/nexus/content/repositories/releases/</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
	</repositories>
	<profiles>
		<profile>
			<id>release</id>

			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-deploy-plugin</artifactId>
						<version>2.8.2</version>
					</plugin>

					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-gpg-plugin</artifactId>
						<version>1.6</version>
						<executions>
							<execution>
								<id>sign-artifacts</id>
								<phase>verify</phase>
								<goals>
									<goal>sign</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
</project>

