/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securityfilter.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * REST management operations on a given security filter instance.
 */
public interface SecurityFilterManagement {

    /**
     * Retrieve runtime information like as availability, selected security context, monitoring, and so on.
     *
     * @return runtime information like as availability, selected security context, monitoring, and so on
     */
    @GET
    @Path("info")
    @Produces({ MediaType.APPLICATION_JSON })
    RuntimeInfo info();

    /**
     * Enable / disable the security filter.
     *
     * @param status the status (enabled / disabled) to set
     */
    @PUT
    @Path("{status}")
    void status(@PathParam("status") Status status);

    /**
     * Push security context, used to adapt the security behavior accordingly.
     * Examples: require / don't require authentication, force strong (X509) authentication, skip policy evaluation /
     * enforcement, deny access, ...
     *
     * @param securityContext JSON string representing security context information
     */
    @POST
    @Path("securityContext")
    @Consumes({ MediaType.APPLICATION_JSON })
    void securityContext(SecurityContext securityContext);

    /**
     * Push the FederationServer URL.
     *
     * @param config FederationServerURL
     */
    @PUT
    @Path("federationServerURL")
    @Consumes({ MediaType.APPLICATION_JSON })
    void federationServerURL(SecurityFilterConfiguration config);
}
