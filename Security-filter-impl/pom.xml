<?xml version="1.0" encoding="UTF-8"?>
<!-- Security Filter Servlet Proxy Copyright (C) 2015 The CHOReVOLUTION project 
	This program is free software: you can redistribute it and/or modify it under 
	the terms of the GNU General Public License as published by the Free Software 
	Foundation, either version 3 of the License, or (at your option) any later 
	version. This program is distributed in the hope that it will be useful, 
	but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
	or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
	more details. You should have received a copy of the GNU General Public License 
	along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<organization>
		<name>The CHOReVOLUTION project</name>
		<url>http://www.chorevolution.eu</url>
	</organization>

	<developers>
		<developer>
			<id>frederic_motte</id>
			<name>Frederic Motte</name>
			<email>frederic.motte@thalesgroup.com</email>
			<organization>Thales communications and security</organization>
			<organizationUrl>https://www.thalesgroup.com/</organizationUrl>
			<timezone>France/Paris</timezone>
		</developer>
	</developers>

	<licenses>
		<license>
			<name>GPL v3.0</name>
			<url>http://www.gnu.org/licenses/gpl-3.0.en.html</url>
			<distribution>repo</distribution>
		</license>
	</licenses>

	<scm>
		<connection>scm:git:ssh://gitolite@tuleap.ow2.org/chorevolution/security-filter.git</connection>
		<developerConnection>scm:git:ssh://gitolite@tuleap.ow2.org/chorevolution/security-filter.git</developerConnection>
		<url>https://tuleap.ow2.org/plugins/git/chorevolution/security-filter</url>
		<tag>HEAD</tag>
	</scm>

	<issueManagement>
		<system>jira</system>
		<url>https://jira.ow2.org/browse/CRV</url>
	</issueManagement>

	<groupId>eu.chorevolution.securityfilter</groupId>
	<artifactId>SecurityfilterServletProxy</artifactId>
	<packaging>war</packaging>
	<version>1.0.0</version>
	<name>SecurityfilterServletProxy Maven Webapp</name>
	<url>http://www.chorevolution.eu</url>
	<description>CHOReVOLUTION Security filter</description>
	<distributionManagement>
		<repository>
			<id>ow2-nexus-releases</id>
			<name>OW2 Release Repository</name>
			<url>http://repository.ow2.org/nexus/service/local/staging/deploy/maven2/</url>
		</repository>
		<snapshotRepository>
			<id>ow2-nexus-snapshots</id>
			<name>OW2 Snapshots Repository</name>
			<url>http://repository.ow2.org/nexus/content/repositories/snapshots/</url>
		</snapshotRepository>
	</distributionManagement>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

		<!-- works with v4.1 and forward; see .travis.yml -->
		<httpclient.version>4.5.1</httpclient.version>
		<!-- the last version to provide LocalTestServer.java -->
		<httpclient.test.version>4.3.5</httpclient.test.version>
		<jax.ws.rs>2.0.1</jax.ws.rs>
		<springmvc>4.1.4.RELEASE</springmvc>
		<cxf.version>3.1.4</cxf.version>
		<jackson.version>1.9.12</jackson.version>
		<!-- <httpclient.version>3.1</httpclient.version> -->
	</properties>

	<dependencies>
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>javax.servlet-api</artifactId>
			<version>3.1.0</version>
		</dependency>

		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>servlet-api</artifactId>
			<version>2.5</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>httpclient</artifactId>
			<version>${httpclient.version}</version>
		</dependency>
		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>2.4</version>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.12</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>httpclient</artifactId>
			<version>${httpclient.test.version}</version>
			<classifier>tests</classifier>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.logging.log4j</groupId>
			<artifactId>log4j-api</artifactId>
			<version>2.4.1</version>
		</dependency>
		<dependency>
			<groupId>org.apache.logging.log4j</groupId>
			<artifactId>log4j-core</artifactId>
			<version>2.4.1</version>
		</dependency>
		<dependency>
			<groupId>org.apache.ws.security</groupId>
			<artifactId>wss4j</artifactId>
			<version>1.6.19</version>
		</dependency>
		<dependency>
			<groupId>eu.chorevolution.securityfilter</groupId>
			<artifactId>sf-provision-data</artifactId>
			<version>1.0.0</version>
		</dependency>
		<dependency>
			<groupId>eu.chorevolution.sts</groupId>
			<artifactId>sts-provision-data</artifactId>
			<version>1.0.0</version>
		</dependency>
		<!-- <dependency> <groupId>com.sun.jersey</groupId> <artifactId>jersey-server</artifactId> 
			<version>1.9</version> </dependency> <dependency> <groupId>com.sun.jersey</groupId> 
			<artifactId>jersey-servlet</artifactId> <version>1.12</version> </dependency> -->
		<dependency>
			<groupId>com.sun.jersey</groupId>
			<artifactId>jersey-client</artifactId>
			<version>1.9</version>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
			<version>${springmvc}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-web</artifactId>
			<version>${springmvc}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-webmvc</artifactId>
			<version>${springmvc}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-ws-security</artifactId>
			<version>3.1.4</version>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-frontend-jaxws</artifactId>
			<version>3.1.4</version>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-frontend-jaxrs</artifactId>
			<version>3.1.4</version>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-transports-http</artifactId>
			<version>${cxf.version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-rs-extension-providers</artifactId>
			<version>${cxf.version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-rs-extension-search</artifactId>
			<version>${cxf.version}</version>
		</dependency>
		<dependency>
			<groupId>javax.ws.rs</groupId>
			<artifactId>javax.ws.rs-api</artifactId>
			<version>${jax.ws.rs}</version>
		</dependency>
		<!-- <dependency> <groupId>org.codehaus.jackson</groupId> <artifactId>jackson-jaxrs</artifactId> 
			<version>${jackson.version}</version> </dependency> -->

		<dependency>
			<groupId>org.apache.cxf.services.sts</groupId>
			<artifactId>cxf-services-sts-core</artifactId>
			<version>3.1.4</version>
		</dependency>
		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-classic</artifactId>
			<version>1.1.3</version>
		</dependency>
		<!-- <dependency> <groupId>eu.chorevolution</groupId> <artifactId>SecurityTokenService</artifactId> 
			<version>0.0.1-SNAPSHOT</version> </dependency> -->
		<dependency>
			<groupId>org.ow2.authzforce</groupId>
			<artifactId>authzforce-ce-xacml-model</artifactId>
			<version>3.4.0</version>
		</dependency>
		<dependency>
			<groupId>org.ow2.authzforce</groupId>
			<artifactId>authzforce-ce-core</artifactId>
			<version>6.0.0</version>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
			<version>2.8.3</version>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.jaxrs</groupId>
			<artifactId>jackson-jaxrs-json-provider</artifactId>
			<version>2.8.3</version>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-rs-client</artifactId>
			<version>${cxf.version}</version>
		</dependency>
		<dependency>
			<groupId>org.codehaus.jettison</groupId>
			<artifactId>jettison</artifactId>
			<version>1.3.8</version>
		</dependency>

		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>3.0</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/com.google.code.gson/gson -->
		<dependency>
			<groupId>com.google.code.gson</groupId>
			<artifactId>gson</artifactId>
			<version>2.7</version>
		</dependency>
		<dependency>
			<groupId>org.bouncycastle</groupId>
			<artifactId>bcprov-ext-jdk15on</artifactId>
			<version>1.55</version>
		</dependency>
		<dependency>
			<groupId>eu.chorevolution.securityfilter</groupId>
			<artifactId>sf-authN-interface</artifactId>
			<version>1.0.0</version>
		</dependency>
		<!-- <dependency> <groupId>org.codehaus.jackson</groupId> <artifactId>jackson-jaxrs</artifactId> 
			<version>1.9.0</version> </dependency> -->
	</dependencies>
	<build>
		<finalName>SecurityfilterServletProxy</finalName>
		<plugins>
			<plugin><!-- Plugin Maven pour creer archive WAR -->
				<artifactId>maven-war-plugin</artifactId>
				<version>2.6</version>
			</plugin>
			<plugin><!-- Plugin pour compilation code Java -->
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.5.1</version>
				<configuration>
					<!-- Java version for compiling the source code -->
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<executions>
					<execution>
						<id>attach-sources</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<executions>
					<execution>
						<id>attach-javadocs</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

		</plugins>
	</build>

	<repositories>
		<repository>
			<id>sonatype</id>
			<url>https://oss.sonatype.org/content/repositories/snapshots</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>ow2-snapshots</id>
			<url>http://repository.ow2.org/nexus/content/repositories/snapshots/</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>ow2-releases</id>
			<url>http://repository.ow2.org/nexus/content/repositories/releases/</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
	</repositories>
</project>
