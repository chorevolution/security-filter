/**
 *   Copyright 2015 The CHOReVOLUTION project
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package eu.chorevolution.rest;

import eu.chorevolution.securityfilter.api.RuntimeInfo;
import eu.chorevolution.securityfilter.api.SecurityContext;
import eu.chorevolution.securityfilter.api.SecurityFilterConfiguration;
import eu.chorevolution.securityfilter.api.SecurityFilterManagement;
import eu.chorevolution.securityfilter.api.Status;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

@Path("/management")
public class SecurityFilterManagementImpl {

    private static final Logger logger = LoggerFactory.getLogger(SecurityFilterManagementImpl.class);

    @Context
    private ServletContext context;
    
    public void init(){
    	
    }
    
    @GET
    @Path("/infos")
    @Produces({ MediaType.APPLICATION_JSON })
    public RuntimeInfo info() {
        logger.debug("RuntimeInfo");
        RuntimeInfo f = new RuntimeInfo();
        f.setSecurityContext((SecurityContext) context.getAttribute("SecurityContext"));
        if (context.getAttribute("Status") != null) {
            f.setStatus(Status.valueOf((String) context.getAttribute("Status")));
        }
        return f;
    }

    
    @PUT
    @Path("/{status}")
    public void status(@PathParam("status")Status status) {
        logger.debug("status {}", status);
        context.setAttribute("Status", status.name());
    }
    
    @POST
    @Path("/securityContext")
    @Consumes({ MediaType.APPLICATION_JSON })
    public void securityContext(String securityContext) {
        logger.debug("securityContext");
        logger.debug("securityContext {}", securityContext);
        try {
			JSONObject obj = new JSONObject(securityContext);
			String securityContextFromJson = obj.getString("SecurityContext");
			try {
			SecurityContext contextsec = SecurityContext.valueOf(securityContextFromJson);
			context.setAttribute("SecurityContext", contextsec);
			} catch(Exception e){
				e.printStackTrace();
				context.setAttribute("SecurityContext", null);
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    /**
     * Push FederationServerURLL
     */
    
    @PUT
    @Path("federationServerURL")
    @Consumes({ MediaType.APPLICATION_JSON })
    public void federationServerURL(SecurityFilterConfiguration config){
        logger.debug("FederationServerURL {}", config.getURL());
        context.setAttribute("FederationServerURL", config.getURL());

    }
}
