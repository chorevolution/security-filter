/**
 *   Copyright 2015 The CHOReVOLUTION project
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package eu.chorevolution;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.ws.security.WSPasswordCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * CommonCallbackHandler
 * @author Frederic Motte
 *
 */
public class CommonCallbackHandler implements CallbackHandler {
	private static final Logger logger = LoggerFactory.getLogger(CommonCallbackHandler.class);

	@Override
	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
		// TODO Auto-generated method stub
		logger.debug("CommonCallbackHandler handle");

		for (int i = 0; i < callbacks.length; i++) { 
			if (callbacks[i] instanceof WSPasswordCallback) { // CXF 
				WSPasswordCallback pc = (WSPasswordCallback) callbacks[i]; 
				logger.debug("CommonCallbackHandler identifier" + pc.getIdentifier());
				logger.debug("CommonCallbackHandler password" + pc.getPassword());
			} 
		} 
	}
}
