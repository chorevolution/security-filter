/**
 *   Copyright 2015 The CHOReVOLUTION project
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package eu.chorevolution;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeader;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.soap.SOAPFaultException;

import org.apache.ws.security.WSConstants;
import org.apache.ws.security.WSSConfig;
import org.apache.ws.security.WSSecurityEngine;
import org.apache.ws.security.WSSecurityEngineResult;
import org.apache.ws.security.WSSecurityException;
import org.apache.ws.security.WSUsernameTokenPrincipal;
import org.apache.ws.security.handler.RequestData;
import org.apache.ws.security.util.WSSecurityUtil;
import org.apache.xml.security.c14n.Canonicalizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import eu.chorevolution.configuration.SecurityPolicy;
import eu.chorevolution.securityfilter.api.SFServletRequestWrapper;
import eu.chorevolution.securityfilter.api.SecurityContext;
import eu.chorevolution.securityfilter.api.Status;

/**
 * Servlet Filter implementation class Securityfilter
 * @author Frederic Motte
 *
 */
public class SecurityFilter implements Filter {

	private static final Logger logger = LoggerFactory.getLogger(SecurityFilter.class);

	MessageFactory messageFactory = null;

	private FilterConfig config;

	private Status currentStatus = null;

	private String STSUrl="";

	SecurityPolicy policy = null;

	File xacmlConfigurationFile = null;

	private WSSecurityEngine securityEngine = null; 

	private UsernameTokenValidator usernameTokenValidator = null;

	private ProtectionAuthNService proService = null;

	private AdaptationAuthNService adaptService = null;

	private QName secHeaderName = new QName(
			"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd",
			"Security", "wsse");

	/**
	 * Default constructor. 
	 * @throws ServletException 
	 */
	public SecurityFilter() throws ServletException {
		// TODO Auto-generated constructor stub
		try {
			// Initialize it to the default.
			messageFactory = MessageFactory.newInstance();

			System.setProperty("javax.xml.accessExternalSchema", "all");
		}
		catch( SOAPException ex ) {
			throw new ServletException( "Unable to create message factory"
					+ ex.getMessage() );
		}
	}

	/**
	 * @see Filter#destroy()
	 */
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
	}


	/**
	 * Return header from the request
	 * @param request
	 * @return
	 */
	private MimeHeaders getHeaders(HttpServletRequest request) {

		Enumeration<?> headerNames = request.getHeaderNames();
		MimeHeaders headers = new MimeHeaders();

		while (headerNames.hasMoreElements()) {
			String headerName = (String)headerNames.nextElement();
			String headerValue = request.getHeader(headerName);

			StringTokenizer values = new StringTokenizer(headerValue, ",");
			while (values.hasMoreTokens()) {
				headers.addHeader(headerName, values.nextToken().trim());
			}
		}
		return headers;
	}

	/**
	 * Transform SaopMessage to Document
	 * @param soapMsg
	 * @return
	 * @throws TransformerConfigurationException
	 * @throws TransformerException
	 * @throws SOAPException
	 * @throws IOException
	 */
	public Document toDocument(SOAPMessage soapMsg) 
			throws TransformerConfigurationException, TransformerException, SOAPException, IOException {
		Source src = soapMsg.getSOAPPart().getContent();
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		DOMResult result = new DOMResult();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.transform(src, result);
		return (Document)result.getNode();
	}

	public static SOAPMessage toSOAPMessage(Document doc) throws Exception {
		Canonicalizer c14n =
				Canonicalizer.getInstance(Canonicalizer.ALGO_ID_C14N_WITH_COMMENTS);
		byte[] canonicalMessage = c14n.canonicalizeSubtree(doc);
		ByteArrayInputStream in = new ByteArrayInputStream(canonicalMessage);
		MessageFactory factory = MessageFactory.newInstance();
		return factory.createMessage(null, in);
	}

	public static void ElementToStream(Element element, OutputStream out) {
		try {
			DOMSource source = new DOMSource(element);
			StreamResult result = new StreamResult(out);
			TransformerFactory transFactory = TransformerFactory.newInstance();
			Transformer transformer = transFactory.newTransformer();
			transformer.transform(source, result);
		} catch (Exception ex) {
		}
	}

	public static String DocumentToString(Document doc) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ElementToStream(doc.getDocumentElement(), baos);
		return new String(baos.toByteArray());
	}
	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		logger.debug("Into the filter");
		HttpServletResponse httpResponse = (HttpServletResponse)response;

		if (config == null)
		{
			logger.error("config is null");
		}

		// check if the security filter is configured
		ServletContext context = config.getServletContext();
		if (context.getAttribute("Status") == null){
			//if not, throw an exception
			httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN, "Security Filter not initialised");
			return;
		}

		if (usernameTokenValidator!=null){
			usernameTokenValidator.setContext(context);
		}
		SecurityContext cont = (SecurityContext)context.getAttribute("SecurityContext");

		if (context.getAttribute("SecurityContext")!= null){
			logger.debug("Into the filter SecurityContext" + cont.toString());
			if (SecurityContext.DENY_ACCESS.equals((SecurityContext)context.getAttribute("SecurityContext"))){
				//if not, throw an exception
				httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "The access to the service is unauthorized");
				return;
			}

			if (SecurityContext.NOT_ENFORCED.equals((SecurityContext)context.getAttribute("SecurityContext"))){
				//if not, throw an exception
				chain.doFilter(request, response);
				return;
			}
		}


		//		if (context.getAttribute("FederationServerURL") !=null){
		//			STSUrl = (String) context.getAttribute("FederationServerURL");
		//		}
		//		else{
		//			httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN, "FederationServer URL not specified");
		//			return;
		//		}
		if (policy == null){
			httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN, "Security Filter has not configuration file");
			return;

		}


		// check configuration compatibility
		// if no consumer part are defined and a provider part is present without default credential : throw an error
		if ((policy.getConsumer()==null)&&(policy.getProvider()!=null))
		{
			if (policy.getProvider().getCredential()==null){
				httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN, "Security Filter configuration file contains incompatible configuration (no consumer and no default credential)");
				return;
			}
		}

		// for a consumer, only user are supported
		if (policy.getConsumer()!=null){
			if (!policy.getConsumer().getCredentialType().equalsIgnoreCase("user")){
				logger.debug("Consumer type" + policy.getConsumer().getCredentialType());
				httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN, "Security Filter configuration file contains incompatible configuration (only user consumer type are supported");
				return;
			}
		} else{
			logger.debug("No consumer part into the configuration file");
			httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN, "Security Filter configuration file contains incompatible configuration (no consumer part)");
			return;

		}




		String userName = null;
		//create multi read inputstream
		MultiReadHttpServletRequest multiReadRequest = new MultiReadHttpServletRequest((HttpServletRequest) request);


		if(context.getAttribute("Status")!=null){
			logger.debug("Current status " + context.getAttribute("Status"));
			currentStatus = Status.valueOf((String) context.getAttribute("Status"));
		}

		try {

			// check if the Security Filter is enabled
			if (currentStatus.equals(Status.ENABLED)){



				if (policy.getProvider().getSFtype().equalsIgnoreCase("CUSTOM_PROTECTION")){
					logger.debug("It's a CUSTOM_PROTECTION");
					boolean res = proService.checkProtectionAuthN(multiReadRequest);
					if (!res){
						logger.debug("The plugin return false -> Authentication Failed ");
						//the authentication is not valid
						throw new WSSecurityException(" invalid credential");
					}else
					{
						logger.debug("The plugin return true -> Authentication OK ");
						chain.doFilter(multiReadRequest, response);
					}
				} else if (policy.getProvider().getSFtype().equalsIgnoreCase("CUSTOM_ADAPTATION")){
					logger.debug("It's a CUSTOM_ADAPTATION");
					SFServletRequestWrapper httpReq = new SFServletRequestWrapper((HttpServletRequest)multiReadRequest);				
					ServletRequest res = adaptService.checkAdaptationAuthN(httpReq);
					logger.debug("-> Forward the request");
					chain.doFilter(res, response);

				} else {
					
					if (policy.getDomain() == null){
						httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN, "Security Filter has not domain defined");
						return;

					}
					// Get all the headers from the HTTP request.
					MimeHeaders headers2 = getHeaders(multiReadRequest);

					// Get the body of the HTTP request.
					ServletInputStream is = multiReadRequest.getInputStream();
					//logger.debug(getStringFromInputStream(is));
					// Now internalize the contents of a HTTP request and
					// create a SOAPMessage
					SOAPMessage msg = messageFactory.createMessage(headers2, is);
					SOAPHeader soapHeader= msg.getSOAPPart().getEnvelope().getHeader();


					// get security Header
					Element e = WSSecurityUtil.getSecurityHeader(toDocument(msg), "");

					if (e==null){
						// no security header
						logger.debug("No security Header into the request");
						//throw new WSSecurityException("No security Header into the request");
					}
					else
					{
						logger.debug("Security header present into the request");
						//}

						// validate the security token present into the SOAPHeader
						RequestData reqData=new RequestData();

						//					// specify the validator class for the usernametoken
						//					wssConfig.setValidator(WSSecurityEngine.USERNAME_TOKEN, new UsernameTokenValidator(STSUrl, policy, xacmlConfigurationFile, context) );
						reqData.setCallbackHandler(new CommonCallbackHandler()); 
						List<WSSecurityEngineResult> res = securityEngine.processSecurityHeader(e, reqData);

						Object principal = null;

						logger.debug("Security Header size " + res.size());
						for (Iterator<WSSecurityEngineResult> iterator = res.iterator(); iterator.hasNext();) {
							WSSecurityEngineResult wsSecurityEngineResult = (WSSecurityEngineResult) iterator.next();

							Integer actInt = (Integer)wsSecurityEngineResult.get(WSSecurityEngineResult.TAG_ACTION);
							if (actInt == WSConstants.UT)
							{
								//since we already have the result and the result contains the username and
								//password, its easiest to just grab the data here and store it for later.
								principal = wsSecurityEngineResult.get(WSSecurityEngineResult.TAG_PRINCIPAL);

							}

						}


						// if the provider is not null, we need to have a security header as output, else do nothing
						// if the provider type is a user, do nothing
						// else if the provider is a service
						//	check is a generic account is present, 
						//  is not, the usernametokenvalidator musts return a service credential for that

						if (policy.getProvider()!=null)
						{
							logger.debug("check the provider type");
							if (policy.getProvider().getSFtype().equalsIgnoreCase("ADAPTATION")){
								logger.debug("It's a ADAPTION");
								logger.debug("remove the security header");
								// remove the security soap header
								Iterator<SOAPHeaderElement> itr2 = soapHeader.examineAllHeaderElements();

								while (itr2.hasNext()) {
									SOAPHeaderElement ele = itr2.next();
									javax.xml.namespace.QName headerName = ele.getElementQName();
									logger.debug("headerName " + headerName.getLocalPart());
									logger.debug("actor " + ele.getActor());
									if (headerName.getLocalPart().equalsIgnoreCase("Security")) {

										logger.debug("remove the security header attributes");
										soapHeader.removeChild(ele);
									}

								}




								logger.debug("recreate the security header attributes");
								//add security attributes
								QName security = new QName("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", 
										"Security");

								SOAPElement Security= msg.getSOAPHeader().addChildElement(security);
								SOAPElement UsernameToken= Security.addChildElement(new QName("UsernameToken"));
								SOAPElement Username= UsernameToken.addChildElement(new QName("Username"));
								SOAPElement Password= UsernameToken.addChildElement(new QName("Password"));

								Password.addAttribute(new QName("Type"), WSConstants.PASSWORD_TEXT);


								if (policy.getProvider().getCredential()!=null && (policy.getProvider().getCredential().getGenericAccount()!= null) && (policy.getProvider().getCredential().getGenericCredential()!= null))
								{
									logger.debug("The credential used are the credential coming from the configuration");
									//enter the username and password coming from the configuration
									Encryptor enc = new Encryptor("dsadsadasa4444");
									logger.debug("name" + policy.getProvider().getCredential().getGenericAccount());
									logger.debug("password" + enc.decode(policy.getProvider().getCredential().getGenericCredential()));
									Username.addTextNode(policy.getProvider().getCredential().getGenericAccount());
									Password.addTextNode(enc.decode(policy.getProvider().getCredential().getGenericCredential()));
								}else{
									//enter the username and password coming from the STS
									if (principal != null && principal instanceof WSUsernameTokenPrincipal)
									{
										logger.debug("The credential used are the credential coming from the STS");
										WSUsernameTokenPrincipal wsUsernameTokenPrincipal = (WSUsernameTokenPrincipal)principal;
										logger.debug("name" + wsUsernameTokenPrincipal.getName());
										logger.debug("password" + wsUsernameTokenPrincipal.getPassword());
										Username.addTextNode(wsUsernameTokenPrincipal.getName());
										Password.addTextNode(wsUsernameTokenPrincipal.getPassword());
									}else{
										throw new WSSecurityException("No credential available to map into the request");
									}
								}

							} else {
								logger.debug("It's Protection : forward the request without modification");
							}
						}//added
					} 



					ByteArrayOutputStream out2 = new ByteArrayOutputStream();
					msg.saveChanges();
					msg.writeTo(out2);
					int length = 0;
					Iterator iterMins2 = msg.getMimeHeaders().getAllHeaders();
					while (iterMins2.hasNext()) {
						MimeHeader object = (MimeHeader) iterMins2.next();
						logger.debug(object.getName() + " " + object.getValue());
						if (object.getName().equalsIgnoreCase("content-length")){
							length = Integer.parseInt(object.getValue());
						}
					}

					String strMsg2 = new String(out2.toByteArray());
					logger.debug("beging message forwarded \n" + strMsg2  + "\n end message forwarded");

					ServletByteArrayInputStream byteArrayInputStream = new ServletByteArrayInputStream(strMsg2);

					InputStreamModifiedRequestWrapper requestWrapper = new InputStreamModifiedRequestWrapper((HttpServletRequest) multiReadRequest);
					requestWrapper.setInputStream(byteArrayInputStream); 
					requestWrapper.setContentLength(length);				

					// pass the request along the filter chain
					chain.doFilter(requestWrapper, response);
				}
			}

		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			httpResponse.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			httpResponse.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
		} catch (WSSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN, e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			httpResponse.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			httpResponse.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			httpResponse.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
		} 
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
		logger.debug("Init Filter");
		this.config = fConfig;
		config.getServletContext().setAttribute("Status", Status.ENABLED.toString());
		STSUrl = config.getInitParameter("STS-URL"); 

		config.getServletContext().setAttribute("FederationServerURL", STSUrl);
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(SecurityPolicy.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			InputStream is = config.getServletContext().getResourceAsStream("/WEB-INF/config.xml");
			policy = (SecurityPolicy)jaxbUnmarshaller.unmarshal(is);

			if (policy.getProvider().getSFtype().equalsIgnoreCase("ADAPTATION")||policy.getProvider().getSFtype().equalsIgnoreCase("PROTECTION")){
				// create WSSecurity engine
				securityEngine=new WSSecurityEngine();
				WSSConfig wssConfig = WSSConfig.getNewInstance();
				securityEngine.setWssConfig(wssConfig);
				usernameTokenValidator = new UsernameTokenValidator(STSUrl, policy, xacmlConfigurationFile);
				// specify the validator class for the usernametoken
				wssConfig.setValidator(WSSecurityEngine.USERNAME_TOKEN, usernameTokenValidator);
			}else{
				proService = ProtectionAuthNService.getInstance();
				adaptService = AdaptationAuthNService. getInstance();
			}
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		URL url;
		try {
			url = config.getServletContext().getResource("/WEB-INF/pdp.xml");
			xacmlConfigurationFile = new File(url.toURI());

			if(!xacmlConfigurationFile.exists()){
				xacmlConfigurationFile = null;
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}




	}



	/**
	 * Create SOAP Fault Exception
	 * @param faultString
	 * @param wsseFaultCode
	 * @return
	 */
	public static SOAPFaultException createSOAPFaultException(
			String faultString, String wsseFaultCode) {

		SOAPFault soapFault;
		try {
			SOAPFactory soapFactory = SOAPFactory.newInstance();
			soapFault = soapFactory
					.createFault(
							faultString,
							new QName(
									"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd",
									wsseFaultCode, "wsse"));
		} catch (SOAPException e) {
			throw new RuntimeException("SOAP error");
		}
		return new SOAPFaultException(soapFault);
	}


}
