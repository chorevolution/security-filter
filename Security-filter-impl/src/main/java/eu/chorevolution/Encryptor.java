/**
 *   Copyright 2015 The CHOReVOLUTION project
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package eu.chorevolution;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang3.ArrayUtils;

public class Encryptor {

	private static String algo ="AES";
	private SecretKeySpec keySpec = null;

	private Cipher encryptCipher = null;
	private Cipher decryptCipher = null;
	
	
	public Encryptor(String secretKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
		
		
		//System.out.println("CREATE ENCRYPTOR WITH : " + secretKey);
        String actualKey = secretKey;
        if (actualKey.length() < 16) {
            StringBuilder actualKeyPadding = new StringBuilder(actualKey);
            for (int i = 0; i < 16 - actualKey.length(); i++) {
                actualKeyPadding.append('0');
            }
            actualKey = actualKeyPadding.toString();
        }
        

		try {
			keySpec = new SecretKeySpec(ArrayUtils.subarray(
			        actualKey.getBytes("UTF-8"), 0, 16), algo);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		encryptCipher = Cipher.getInstance("AES");
		encryptCipher.init(Cipher.ENCRYPT_MODE, keySpec);
		decryptCipher = Cipher.getInstance("AES");
		decryptCipher.init(Cipher.DECRYPT_MODE, keySpec);
	}




	public String encode(final String value) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException{
		String encodedValue = null;
		if (value != null) {
//			final Cipher cipher = Cipher.getInstance("AES");
//			cipher.init(Cipher.ENCRYPT_MODE, keySpec);
			byte[] envVal = encryptCipher.doFinal(value.getBytes("UTF-8"));
			encodedValue = Base64.getEncoder().encodeToString(envVal);
		}
		return encodedValue;
	}

	public String decode(final String encodedValue) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		//System.out.println("encoded value " + encodedValue);
		String value = null;
		if (encodedValue != null){
//			final Cipher cipher = Cipher.getInstance("AES");
//			cipher.init(Cipher.DECRYPT_MODE, keySpec);
			byte[] decodedValue  = Base64.getDecoder().decode(encodedValue);
			byte[] decVal = decryptCipher.doFinal(decodedValue);
			value = new String(decVal, "UTF-8");
		}
		return value;
	}
	
	
	public static void main(String[] args) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException {
		String password = "password";
		Encryptor encryptor = new Encryptor("skjajsjsjljkdsa6633");
		try {
			String encryptedText = encryptor.encode(password);
			String decryptedText = encryptor.decode(encryptedText);
			
			System.out.println("original "  + password);
			System.out.println("encrypted "  + encryptedText);
			System.out.println("decrypted "  + decryptedText);
			
			
			System.out.println(encryptor.decode("AEdnzs++tE4XgTQVE6csCw=="));
			
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
