/**
 *   Copyright 2015 The CHOReVOLUTION project
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package eu.chorevolution;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;

import javax.servlet.ServletContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.cxf.frontend.ClientProxy;

import eu.chorevolution.configuration.SecurityPolicy;
import eu.chorevolution.securityfilter.api.SecurityContext;
import eu.chorevolution.securitytokenservice.SecurityTokenService;
import eu.chorevolution.securitytokenservice.SecurityTokenServiceConstants;
import eu.chorevolution.securitytokenservice.federationserver.api.EndUser;

import org.apache.cxf.ws.security.sts.provider.model.RequestSecurityTokenResponseType;
import org.apache.cxf.ws.security.sts.provider.model.RequestSecurityTokenType;
import org.apache.cxf.ws.security.sts.provider.model.RequestedSecurityTokenType;
import org.apache.cxf.ws.security.sts.provider.model.StatusType;
import org.apache.cxf.ws.security.sts.provider.model.ValidateTargetType;
import org.apache.cxf.ws.security.sts.provider.model.secext.AttributedString;
import org.apache.cxf.ws.security.sts.provider.model.secext.PasswordString;
import org.apache.cxf.ws.security.sts.provider.model.secext.UsernameTokenType;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.WSSecurityException;
import org.apache.ws.security.handler.RequestData;
import org.apache.ws.security.validate.Credential;
import org.apache.ws.security.validate.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.cxf.sts.QNameConstants;
import org.apache.cxf.sts.STSConstants;
import org.apache.cxf.sts.operation.TokenRequestCollectionOperation;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import org.ow2.authzforce.core.pdp.impl.PDPImpl;
import org.ow2.authzforce.core.pdp.impl.PdpConfigurationParser;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.AdviceExpressions;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.AllOf;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.AnyOf;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.Attribute;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.AttributeDesignatorType;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.AttributeSelectorType;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.AttributeValueType;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.Attributes;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.Condition;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.Content;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.DecisionType;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.DefaultsType;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.EffectType;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.Match;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.MultiRequests;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.ObjectFactory;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.ObligationExpressions;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.Policy;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.PolicyIssuer;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.PolicySet;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.Request;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.RequestDefaults;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.Response;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.Result;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.Rule;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.Target;
import org.springframework.util.ResourceUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class UsernameTokenValidator implements Validator {

	public static Object fromDBObject(Object dbObj, Class clazz) {
		String json = dbObj.toString();
		ObjectMapper om = new ObjectMapper();
		try {
			Object o = om.readValue(json, clazz);
			logger.info("json output" + json);
			return o;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * XACML policy filename used by default when no PDP configuration file found, i.e. no file named
	 * {@value #PDP_CONF_FILENAME} exists in the test directory
	 */
	public final static String POLICY_FILENAME = "policy.xml";

	/**
	 * PDP Configuration file name
	 */
	public final static String PDP_CONF_FILENAME = "pdp.xml";

	/**
	 * Spring-supported location to XML catalog (may be prefixed with classpath:, etc.)
	 */
	public final static String XML_CATALOG_LOCATION = "classpath:catalog.xml";
	private static final QName QNAME_WST_STATUS = 
			QNameConstants.WS_TRUST_FACTORY.createStatus(null).getName();
	private String sTSUrl;
	private String domain;
	//	private String domainIssuer;

	private static final Logger logger = LoggerFactory.getLogger(UsernameTokenValidator.class);

	private SecurityPolicy policy = null;
	private File xacmlConfigurationFile = null;

	private ServletContext context = null;
	private SecurityTokenService clientSecurityTokenService = null;
	/**
	 * @param sTSUrl
	 * @param policy 
	 * @param xacmlConfigurationFile
	 */
	public UsernameTokenValidator(String sTSUrl , SecurityPolicy policy, File xacmlConfigurationFile) {
		// TODO Auto-generated constructor stub
		this.sTSUrl = sTSUrl;
		this.policy = policy;
		this.domain = policy.getDomain();
		this.xacmlConfigurationFile = xacmlConfigurationFile;
		//this.context = context;
		//		this.domainIssuer = policy.getDomain();

		URL wsdlURL;
		try {
			wsdlURL = new URL(sTSUrl + "/services/securitytokenservice?wsdl");
			QName SERVICE_NAME = new QName("http://docs.oasis-open.org/ws-sx/ws-trust/200512/wsdl", "SecurityTokenServiceImplService");
			Service service = Service.create(wsdlURL, SERVICE_NAME);

			clientSecurityTokenService = service.getPort(SecurityTokenService.class);

			ClientProxy.getClient(clientSecurityTokenService).getInInterceptors().add(new LoggingInInterceptor());
			ClientProxy.getClient(clientSecurityTokenService).getOutInterceptors().add(new LoggingOutInterceptor());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	@Override
	public Credential validate(Credential credential, RequestData data) throws WSSecurityException {

		if (credential == null || credential.getUsernametoken() == null)
			throw new WSSecurityException("No credential found"); 

		// TODO Auto-generated method stub
		// logger.debug("Validate credential " + credential.getPrincipal().getName());

		//logger.debug("name" + credential.getSecurityContextToken().getTokenType());
		String originUsername = credential.getUsernametoken().getName();
		logger.debug("name" + credential.getUsernametoken().getName());
		logger.debug("password type" + credential.getUsernametoken().getPasswordType());
		logger.debug("password" + credential.getUsernametoken().getPassword());



		logger.debug("invoke STS webservice...");
		RequestSecurityTokenType request = new RequestSecurityTokenType();


		if (policy.getProvider()!=null && policy.getProvider().getCredential()!=null 
				&& (policy.getProvider().getCredential().getGenericAccount()==null || policy.getProvider().getCredential().getGenericCredential()==null) 
				&& policy.getProvider().getSFtype().equalsIgnoreCase("ADAPTATION")){
			logger.debug("create a issue request");

			ConsumerProviderType consumerType = null;
			ConsumerProviderType providerType = null;
			String providerName = null;
			if (policy.getConsumer().getCredentialType()!=null){
				logger.debug("consumer type " + policy.getConsumer().getCredentialType().toUpperCase());
				consumerType = ConsumerProviderType.valueOf( policy.getConsumer().getCredentialType().toUpperCase());
			}
			if (policy.getProvider().getCredential()!=null){
				logger.debug("provider type " + policy.getProvider().getCredential().getAuthNTypeForwarded().toUpperCase());
				if (policy.getProvider().getCredential().getAuthNTypeForwarded().equalsIgnoreCase("UserAccount")){
					providerType = ConsumerProviderType.USER;
				}else
				{
					providerType = ConsumerProviderType.SERVICE;
				}
			}
			providerName = policy.getProvider().getServiceName();


			// it's an issuer request
			JAXBElement<String> requestType = 
					new JAXBElement<String>(
							QNameConstants.REQUEST_TYPE, String.class, TokenRequestCollectionOperation.WSTRUST_REQUESTTYPE_BATCH_ISSUE
							);
			request.getAny().add(requestType);


			JAXBElement<String> tokenType = 
					new JAXBElement<String>(
							QNameConstants.TOKEN_TYPE, String.class, WSConstants.WSS_USERNAME_TOKEN_VALUE_TYPE
							);
			request.getAny().add(tokenType);

			request.getOtherAttributes().put(SecurityTokenServiceConstants.SECURITY_ISSUER_DOMAIN, domain);
			if (consumerType != null){
				request.getOtherAttributes().put(SecurityTokenServiceConstants.SECURITY_CONSUMER_TYPE, consumerType.toString());
			}

			if (providerType != null){
				request.getOtherAttributes().put(SecurityTokenServiceConstants.SECURITY_PROVIDER_TYPE, providerType.toString());
			}
			if (providerName != null){
				request.getOtherAttributes().put(SecurityTokenServiceConstants.SECURITY_PROVIDER_NAME, providerName);
			}

		} else {
			// it's a validate request
			logger.debug("create a validate request");

			ConsumerProviderType consumerType = null;
			if (policy.getConsumer().getCredentialType()!=null){
				consumerType = ConsumerProviderType.valueOf( policy.getConsumer().getCredentialType().toUpperCase());
			}

			if (consumerType != null){
				request.getOtherAttributes().put(SecurityTokenServiceConstants.SECURITY_CONSUMER_TYPE, consumerType.toString());
			}

			JAXBElement<String> requestType = 
					new JAXBElement<String>(
							QNameConstants.REQUEST_TYPE, String.class, TokenRequestCollectionOperation.WSTRUST_REQUESTTYPE_BATCH_VALIDATE
							);
			request.getAny().add(requestType);

			JAXBElement<String> tokenType = 
					new JAXBElement<String>(
							QNameConstants.TOKEN_TYPE, String.class, STSConstants.STATUS
							);
			request.getAny().add(tokenType);

		}

		JAXBElement<UsernameTokenType> usernameTokenType = createUsernameToken(credential.getUsernametoken().getName(),
				credential.getUsernametoken().getPassword(),
				credential.getUsernametoken().getPasswordType(),
				domain);

		ValidateTargetType validateTarget = new ValidateTargetType();
		validateTarget.setAny(usernameTokenType);

		JAXBElement<ValidateTargetType> validateTargetType = 
				new JAXBElement<ValidateTargetType>(
						QNameConstants.VALIDATE_TARGET, ValidateTargetType.class, validateTarget
						);
		request.getAny().add(validateTargetType);

		RequestSecurityTokenResponseType responce = null;
		StatusType status = null;
		RequestedSecurityTokenType rstt = null;
		String tokenType = null;


		if (policy.getProvider()!=null && policy.getProvider().getCredential()!=null 
				&& (policy.getProvider().getCredential().getGenericAccount()==null || policy.getProvider().getCredential().getGenericCredential()==null) 
				&& policy.getProvider().getSFtype().equalsIgnoreCase("ADAPTATION")){
			responce = clientSecurityTokenService.issueSingle(request);



			// si token type est un status -> checker le status car il doit etre false
			// sinon c'est le type de token retrouner
			logger.debug("message context is the following :" + responce);
			for (Object requestObject : responce.getAny()) {
				// JAXB types
				if (requestObject instanceof JAXBElement<?>) {
					JAXBElement<?> jaxbElement = (JAXBElement<?>) requestObject;
					logger.debug("Found " + jaxbElement.getName() + ": " + jaxbElement.getValue());


					if (jaxbElement.getValue() instanceof RequestedSecurityTokenType){
						rstt = (RequestedSecurityTokenType) jaxbElement.getValue(); 
					}

					if (QNAME_WST_STATUS.equals(jaxbElement.getName())) {
						status = (StatusType)jaxbElement.getValue();
					}

					if (QNameConstants.TOKEN_TYPE.equals(jaxbElement.getName())) {
						tokenType = (String)jaxbElement.getValue();
					}
				}else{
					logger.debug("error");
					throw new WSSecurityException(" invalid credential");

				}
			}

			// check if a validation status is present
			if (tokenType != null && STSConstants.STATUS.equals(tokenType)){
				if (status !=null){
					if (STSConstants.INVALID_CODE.equals(status.getCode())) {
						logger.debug("is not valid");
						throw new WSSecurityException(" invalid credential");
					} else
					{
						logger.debug("is not a valid response");
						throw new WSSecurityException(" invalid responce");
					}
				}
				else
				{
					logger.debug("is not a valid response");
					throw new WSSecurityException(" invalid responce");

				}
			}
			if (rstt != null)
			{
				if (WSConstants.WSS_USERNAME_TOKEN_VALUE_TYPE.equalsIgnoreCase(tokenType)){
					JAXBElement<UsernameTokenType> token = (JAXBElement<UsernameTokenType> )rstt.getAny();
					logger.debug("user" + token.getValue().getUsername().getValue());

					for (Object requestObject : token.getValue().getAny())
					{
						if (requestObject instanceof JAXBElement<?>) {
							JAXBElement<?> jaxbElement = (JAXBElement<?>) requestObject;
							logger.debug("Found " + jaxbElement.getName() + ": " + jaxbElement.getValue());
							if (QNameConstants.PASSWORD.equals(jaxbElement.getName())){
								PasswordString password = (PasswordString)jaxbElement.getValue();
								logger.debug("password " + password.getValue());
								logger.debug("password type " + password.getType());
								credential.getUsernametoken().setName(token.getValue().getUsername().getValue());
								credential.getUsernametoken().setPassword(password.getValue());
							}
						}
					}
				}
			}
		}
		else
		{
			responce = clientSecurityTokenService.validate(request);
			logger.debug("message context is the following :" + responce);
			for (Object requestObject : responce.getAny()) {
				// JAXB types
				if (requestObject instanceof JAXBElement<?>) {
					JAXBElement<?> jaxbElement = (JAXBElement<?>) requestObject;
					logger.debug("Found " + jaxbElement.getName() + ": " + jaxbElement.getValue());

					if (QNAME_WST_STATUS.equals(jaxbElement.getName())) {
						status = (StatusType)jaxbElement.getValue();
						if (STSConstants.VALID_CODE.equals(status.getCode())) {
							logger.debug("is valid");
							if (domain != null){
								//TODO map credential
							}
						}
						else
						{
							logger.debug("is not valid");
							throw new WSSecurityException(" invalid credential");
						}
					}
				}else{
					logger.debug("error");
					throw new WSSecurityException(" invalid credential");

				}
			}
		}

		logger.debug("*******************************" + context.getAttribute("SecurityContext"));
		if (context.getAttribute("SecurityContext")!= null){
			if (!SecurityContext.AUTHENTICATED_ONLY.equals((SecurityContext)context.getAttribute("SecurityContext"))){
				logger.debug("** Performed the Authorization");
				checkAuthZ(originUsername, this.domain, this.sTSUrl, xacmlConfigurationFile);
			}else{
				logger.debug("*** Don't performed the Authorization");
			}
		} else {
			logger.debug("** Performed the Authorization");
			checkAuthZ(originUsername, this.domain, this.sTSUrl, xacmlConfigurationFile);
		}
		logger.debug("*******************************");

		return credential;
	}



	public ServletContext getContext() {
		return context;
	}

	public void setContext(ServletContext context) {
		this.context = context;
	}

	private void checkAuthZ(String originUsername, String domain, String STSUrl, File xacmlConfigurationFile) throws WSSecurityException {
		// TODO Auto-generated method stub
		PDPImpl pdp;
		try {

			Client client = Client.create();

			WebResource webResource = client
					.resource(STSUrl + "/resources/domains/" + domain + "/endusers/" + originUsername);
			//			webResource.path(domain).path("endusers").path(originUsername);

			logger.debug("call the REST URL : " + webResource.getURI() );
			ClientResponse response = webResource.accept("application/json")
					.get(ClientResponse.class);

			if (response.getStatus() != 200) {
				throw new WSSecurityException("Unauthorized to access the service. No user information found");
			}

			String output = response.getEntity(String.class);

			logger.debug("Output from Server .... \n");
			logger.debug(output);
			//Gson gson = new Gson();

			ObjectMapper om = new ObjectMapper();
			EndUser userRes = om.readValue(output, EndUser.class);
			logger.info("json output" + output);


			//EndUser userRes = gson.fromJson(output, EndUser.class);
			//			System.out.println(userRes.getPassword());
			//			System.out.println(userRes.getUsername());



			pdp = PdpConfigurationParser.getPDP(xacmlConfigurationFile.getAbsolutePath(), XML_CATALOG_LOCATION, null);
			MultiRequests multiRequests = null;
			List<Attributes> attributes = new ArrayList<Attributes>();
			List<Attribute> attributeList = new ArrayList<Attribute>();
			Attributes a1 = new Attributes(null, attributeList, "urn:oasis:names:tc:xacml:1.0:subject-category:access-subject", null);

			List<AttributeValueType> attributeValues = new ArrayList<AttributeValueType>();
			Set<String> UserGroup = userRes.getGroups();
			for (Iterator iterator = UserGroup.iterator(); iterator.hasNext();) {
				String group = (String) iterator.next();
				List<Serializable> content = Collections.<Serializable>singletonList(group);
				AttributeValueType e = new AttributeValueType(content, "http://www.w3.org/2001/XMLSchema#string", Collections.emptyMap());
				attributeValues.add(e);
			}


			Attribute attribute = new Attribute(attributeValues, "group", null, false);
			attributeList.add(attribute);
			boolean returnPolicyIdList = false;
			boolean combinedDecision = false;
			attributes.add(a1);
			Request r = new Request(null, attributes, multiRequests, returnPolicyIdList, combinedDecision);
			//			JAXBContext context = JAXBContext.newInstance(Request.class);
			//			Marshaller jaxbMarshaller = context.createMarshaller();
			//			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			//jaxbMarshaller.marshal(r, System.out);
			//System.out.println(r.toString());
			Response res = pdp.evaluate(r);

			List<Result> result = res.getResults();
			for (Iterator iterator = result.iterator(); iterator.hasNext();) {
				Result result2 = (Result) iterator.next();
				if (!result2.getDecision().equals(DecisionType.PERMIT)){
					throw new WSSecurityException("Unauthorized to access the service");
				}
			}

			//			JAXBContext context2 = JAXBContext.newInstance(Response.class);
			//			Marshaller jaxbmarshaller2 = context2.createMarshaller();
			//			jaxbmarshaller2.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			//jaxbmarshaller2.marshal(res, System.out);
		} catch (IllegalArgumentException  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//		} catch (JAXBException e1) {
			//			// TODO Auto-generated catch block
			//			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	private static JAXBElement<UsernameTokenType> createUsernameToken(String name, String password, String passwordkind, String domain) {
		UsernameTokenType usernameToken = new UsernameTokenType();

		AttributedString username = new AttributedString();
		username.setValue(name);
		usernameToken.setUsername(username);

		// Add a password
		PasswordString passwordString = new PasswordString();
		passwordString.setValue(password);
		//		passwordString.setType(WSConstants.PASSWORD_TEXT);
		passwordString.setType(passwordkind);

		JAXBElement<PasswordString> passwordType = 
				new JAXBElement<PasswordString>(
						QNameConstants.PASSWORD, PasswordString.class, passwordString
						);
		usernameToken.getAny().add(passwordType);

		JAXBElement<UsernameTokenType> tokenType = 
				new JAXBElement<UsernameTokenType>(
						QNameConstants.USERNAME_TOKEN, UsernameTokenType.class, usernameToken
						);

		usernameToken.getOtherAttributes().put(SecurityTokenServiceConstants.SECURITY_VALIDATION_DOMAIN, domain);
		return tokenType;
	}
}

