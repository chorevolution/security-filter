/**
 *   Copyright 2015 The CHOReVOLUTION project
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package eu.chorevolution;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.ws.soap.SOAPFaultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import eu.chorevolution.securityfilter.api.Status;

/**
 * Servlet Filter implementation class SetInvocationAddressFilter
 * @author Frederic Motte
 *
 */
public class SetInvocationAddressFilter implements Filter {

	private static final Logger logger = LoggerFactory.getLogger(SetInvocationAddressFilter.class);

	MessageFactory messageFactory = null;

	private FilterConfig config;

	private Status currentStatus = null;


	/**
	 * Default constructor. 
	 * @throws ServletException 
	 */
	public SetInvocationAddressFilter() throws ServletException {
		// TODO Auto-generated constructor stub
		try {
			// Initialize it to the default.
			messageFactory = MessageFactory.newInstance();
		}
		catch( SOAPException ex ) {
			throw new ServletException( "Unable to create message factory"
					+ ex.getMessage() );
		}
	}

	/**
	 * @see Filter#destroy()
	 */
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
	}


	/**
	 * Return header from the request
	 * @param request
	 * @return
	 */
	private MimeHeaders getHeaders(HttpServletRequest request) {

		Enumeration<?> headerNames = request.getHeaderNames();
		MimeHeaders headers = new MimeHeaders();

		while (headerNames.hasMoreElements()) {
			String headerName = (String)headerNames.nextElement();
			String headerValue = request.getHeader(headerName);
			StringTokenizer values = new StringTokenizer(headerValue, ",");
			while (values.hasMoreTokens()) {
				headers.addHeader(headerName, values.nextToken().trim());
			}
		}
		return headers;
	}

	/**
	 * Transform SaopMessage to Document
	 * @param soapMsg
	 * @return
	 * @throws TransformerConfigurationException
	 * @throws TransformerException
	 * @throws SOAPException
	 * @throws IOException
	 */
	public Document toDocument(SOAPMessage soapMsg) 
			throws TransformerConfigurationException, TransformerException, SOAPException, IOException {
		Source src = soapMsg.getSOAPPart().getContent();
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		DOMResult result = new DOMResult();
		transformer.transform(src, result);
		return (Document)result.getNode();
	}

	private Name getName(SOAPMessage message) {
		Name rvalue = null;
		SOAPPart soap = message.getSOAPPart();
		if (soap != null) {
			try {
				SOAPEnvelope envelope = soap.getEnvelope();
				if (envelope != null) {
					SOAPBody body = envelope.getBody();
					if (body != null) {
						Iterator it = body.getChildElements();
						while (it.hasNext()) {
							Object o = it.next();
							if (o instanceof SOAPElement) {
								rvalue = ((SOAPElement) o).getElementName();

//								JAXBContext context;
//								try {
//									context = JAXBContext.newInstance(SetInvocationAddress.class);
//									Unmarshaller unmarshaller = context.createUnmarshaller();
//
//									JAXBElement<SetInvocationAddress> element = unmarshaller.unmarshal((new DOMResult(((SOAPElement) o)).getNode()),
//											SetInvocationAddress.class);
//									SetInvocationAddress set = element.getValue();
//									logger.debug("getName " + set.getName());
//									logger.debug("getRole " + set.getRole());
//									List<String> l = set.getEndpoints();
//									for (Iterator iterator = l.iterator(); iterator.hasNext();) {
//										String string = (String) iterator.next();
//										logger.debug("getEndpoints " + string);
//										this.config.getServletContext().setAttribute(SecurityFilterConstant.P_TARGET_URI, string);
//									}								  								 								  
//								} catch (JAXBException e) {
//									// TODO Auto-generated catch block
//									e.printStackTrace();
//								}
//
								break;
							}
						}
					}
				}
			} catch (SOAPException se) {

			}
		}
		return rvalue;
	}
	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here
		logger.debug("Into the SetInvocationAddressFilter filter");
		HttpServletResponse httpResponse = (HttpServletResponse)response;

		if (config == null)
		{
			logger.error("config is null");
		}

		MultiReadHttpServletRequest multiReadRequest = new MultiReadHttpServletRequest((HttpServletRequest) request);


		if (multiReadRequest.getQueryString()!=null){
			if (multiReadRequest.getQueryString().equalsIgnoreCase("wsdl")){
				logger.info("It's a wsdl request, Try to forward the request to the setinvocationservice");
				multiReadRequest.getRequestDispatcher("/SecurityFilterManagement/setInvocationAddress").forward(multiReadRequest, response);
				return;
			}
		}

		try {

			// check if the Security Filter is enabled
			// Get all the headers from the HTTP request.
			MimeHeaders headers2 = getHeaders(multiReadRequest);

			// Get the body of the HTTP request.
			ServletInputStream is = multiReadRequest.getInputStream();
			//logger.debug(getStringFromInputStream(is));
			// Now internalize the contents of a HTTP request and
			// create a SOAPMessage
			SOAPMessage msg = messageFactory.createMessage(headers2, is);

			ByteArrayOutputStream out = new ByteArrayOutputStream();
			msg.writeTo(out);
			String strMsg = new String(out.toByteArray());
			logger.debug("message  " + strMsg);
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();


			Name name = getName(msg);
			logger.debug("name" + name.getLocalName());
			if(SecurityFilterConstant.setInvocationAddressName.equals(name.getLocalName())){
				//if(true){

				logger.info("It's a setInvocationAddress request, Try to forward the request to the setinvocationservice");
				multiReadRequest.getRequestDispatcher("/SecurityFilterManagement/setInvocationAddress").forward(multiReadRequest, response);
				return;
			}else{
				// pass the request along the filter chain
				chain.doFilter(multiReadRequest, response);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			httpResponse.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//httpResponse.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
		logger.debug("Init Filter");
		this.config = fConfig;
	}

	/**
	 * Create SOAP Fault Exception
	 * @param faultString
	 * @param wsseFaultCode
	 * @return
	 */
	public static SOAPFaultException createSOAPFaultException(
			String faultString, String wsseFaultCode) {

		SOAPFault soapFault;
		try {
			SOAPFactory soapFactory = SOAPFactory.newInstance();
			soapFault = soapFactory
					.createFault(
							faultString,
							new QName(
									"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd",
									wsseFaultCode, "wsse"));
		} catch (SOAPException e) {
			throw new RuntimeException("SOAP error");
		}
		return new SOAPFaultException(soapFault);
	}

}
