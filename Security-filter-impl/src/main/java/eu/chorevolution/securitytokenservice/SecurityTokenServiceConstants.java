/**
 *   Copyright 2015 The CHOReVOLUTION project
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package eu.chorevolution.securitytokenservice;

import javax.xml.namespace.QName;

public class SecurityTokenServiceConstants {

	public static final QName SECURITY_VALIDATION_DOMAIN = new QName("http://chorevolution.eu/STS" , "SecurityValidationDomain");
	public static final QName SECURITY_ISSUER_DOMAIN = new QName("http://chorevolution.eu/STS" , "SecurityIssuerDomain");

	public static final QName SECURITY_CONSUMER_TYPE = new QName("http://chorevolution.eu/STS" , "SecurityConsumerType");
	public static final QName SECURITY_PROVIDER_TYPE = new QName("http://chorevolution.eu/STS" , "SecurityProviderType");

	public static final QName SECURITY_PROVIDER_NAME = new QName("http://chorevolution.eu/STS" , "SecurityProviderName");

}
