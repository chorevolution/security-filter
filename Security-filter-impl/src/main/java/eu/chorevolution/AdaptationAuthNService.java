package eu.chorevolution;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.ServiceConfigurationError;
import java.util.ServiceLoader;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.securityfilter.api.AdaptationAuthNInterface;
import eu.chorevolution.securityfilter.api.SFServletRequestWrapper;

public class AdaptationAuthNService implements AdaptationAuthNInterface {

	private static final Logger logger = LoggerFactory.getLogger(AdaptationAuthNService.class);

	private static AdaptationAuthNService service;
	private ServiceLoader<AdaptationAuthNInterface> loader;

	private AdaptationAuthNService() {
		loader = ServiceLoader.load(AdaptationAuthNInterface.class);
	}

	public static synchronized AdaptationAuthNService getInstance() {
		if (service == null) {
			service = new AdaptationAuthNService();
		}
		return service;
	}

	@Override
	public SFServletRequestWrapper checkAdaptationAuthN(SFServletRequestWrapper request) {
		// TODO Auto-generated method stub
		SFServletRequestWrapper ReturnRequest = null;
		logger.debug("Call the plugin for the adaptation");

		try {
			Iterator<AdaptationAuthNInterface> dictionaries = loader.iterator();
			while (ReturnRequest == null && dictionaries.hasNext()) {
				AdaptationAuthNInterface d = dictionaries.next();
				ReturnRequest = d.checkAdaptationAuthN(request);
			}
		} catch (ServiceConfigurationError serviceError) {
			ReturnRequest = null;
			serviceError.printStackTrace();

		}
		return ReturnRequest;
	}
}
