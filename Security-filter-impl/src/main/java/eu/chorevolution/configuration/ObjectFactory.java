//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2016.09.12 à 11:00:09 AM CEST 
//


package eu.chorevolution.configuration;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the eu.chorevolution.configuration package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: eu.chorevolution.configuration
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SecurityPolicy }
     * 
     */
    public SecurityPolicy createSecurityPolicy() {
        return new SecurityPolicy();
    }

    /**
     * Create an instance of {@link SecurityPolicy.Provider }
     * 
     */
    public SecurityPolicy.Provider createSecurityPolicyProvider() {
        return new SecurityPolicy.Provider();
    }

    /**
     * Create an instance of {@link SecurityPolicy.Consumer }
     * 
     */
    public SecurityPolicy.Consumer createSecurityPolicyConsumer() {
        return new SecurityPolicy.Consumer();
    }

    /**
     * Create an instance of {@link SecurityPolicy.Provider.Credential }
     * 
     */
    public SecurityPolicy.Provider.Credential createSecurityPolicyProviderCredential() {
        return new SecurityPolicy.Provider.Credential();
    }

}
