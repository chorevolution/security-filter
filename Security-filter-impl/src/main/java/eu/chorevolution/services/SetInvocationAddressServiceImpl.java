/**
 *   Copyright 2015 The CHOReVOLUTION project
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package eu.chorevolution.services;

import java.net.URL; 
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List; 
import javax.annotation.Resource;
import javax.jws.WebMethod; 
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.SecurityFilterConstant; 




@WebService (targetNamespace = "http://services.chorevolution.eu/",
			serviceName = "BaseService",
			portName="ConfigurableServicePort",
			endpointInterface="eu.chorevolution.services.SetInvocationAddressService")
public class SetInvocationAddressServiceImpl implements SetInvocationAddressService { 
	private static final Logger logger = LoggerFactory.getLogger(SetInvocationAddressServiceImpl.class);

	@Resource
	private WebServiceContext context;

	private static List<URL> endpoints = new ArrayList<URL>(); 

	public SetInvocationAddressServiceImpl() { 

		logger.debug("SetInvocationAddressServiceImpl"); 
	} 

	/* (non-Javadoc)
	 * @see org.ow2.choreos.TravelAgencyService#setInvocationAddress(java.lang.String, java.lang.String, java.util.List)
	 */
	@Override
	@WebMethod 
	public void setInvocationAddress(String role, String name, List<String> endpoints) { 
		ServletContext servletContext =
				(ServletContext) context.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
		Enumeration<String> attrs = servletContext.getAttributeNames();
//		List attrList = Collections.list(attrs);
//		for (Iterator iterator = attrList.iterator(); iterator.hasNext();) {
//			String object = (String) iterator.next();
//			System.out.println(object);
//		}

		logger.debug("setting inv. addrr to "+ endpoints); 
		logger.debug("role "+ role); 
		logger.debug("name "+ name); 
		if (endpoints.size()>0){
			logger.info("set the invocatin address to : " +  endpoints.get(0));
			servletContext.setAttribute(SecurityFilterConstant.P_TARGET_URI, endpoints.get(0));
		}
	} 

}

