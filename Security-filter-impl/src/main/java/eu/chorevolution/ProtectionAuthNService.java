package eu.chorevolution;

import java.util.Iterator;
import java.util.ServiceConfigurationError;
import java.util.ServiceLoader;

import javax.servlet.ServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.securityfilter.api.ProtectionAuthNInterface;;

public class ProtectionAuthNService implements ProtectionAuthNInterface {
	private static final Logger logger = LoggerFactory.getLogger(ProtectionAuthNService.class);

	private static ProtectionAuthNService service;
	private ServiceLoader<ProtectionAuthNInterface> loader;

	private ProtectionAuthNService() {
		loader = ServiceLoader.load(ProtectionAuthNInterface.class);
	}

	public static synchronized ProtectionAuthNService getInstance() {
		if (service == null) {
			service = new ProtectionAuthNService();
		}
		return service;
	}

	@Override
	public boolean checkProtectionAuthN(ServletRequest request) {
		// TODO Auto-generated method stub
		boolean result = false;

		logger.debug("Call the plugin for the protection");

		try {
			Iterator<ProtectionAuthNInterface> dictionaries = loader.iterator();
			while (result == false && dictionaries.hasNext()) {
				ProtectionAuthNInterface d = dictionaries.next();
				result = d.checkProtectionAuthN(request);
			}
		} catch (ServiceConfigurationError serviceError) {
			result = false;
			serviceError.printStackTrace();

		}
		return result;
	}    
}
