//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2016.09.12 à 11:00:09 AM CEST 
//


package eu.chorevolution.modelingnotations.configuration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consumer",
    "provider"
})
@XmlRootElement(name = "SecurityPolicy")
public class SecurityPolicy {

    @XmlElement(name = "Consumer", required = true)
    protected SecurityPolicy.Consumer consumer;
    @XmlElement(name = "Provider", required = true)
    protected SecurityPolicy.Provider provider;
    @XmlAttribute(name = "domain")
    protected String domain;

    /**
     * Obtient la valeur de la propriété consumer.
     * 
     * @return
     *     possible object is
     *     {@link SecurityPolicy.Consumer }
     *     
     */
    public SecurityPolicy.Consumer getConsumer() {
        return consumer;
    }

    /**
     * Définit la valeur de la propriété consumer.
     * 
     * @param value
     *     allowed object is
     *     {@link SecurityPolicy.Consumer }
     *     
     */
    public void setConsumer(SecurityPolicy.Consumer value) {
        this.consumer = value;
    }

    /**
     * Obtient la valeur de la propriété provider.
     * 
     * @return
     *     possible object is
     *     {@link SecurityPolicy.Provider }
     *     
     */
    public SecurityPolicy.Provider getProvider() {
        return provider;
    }

    /**
     * Définit la valeur de la propriété provider.
     * 
     * @param value
     *     allowed object is
     *     {@link SecurityPolicy.Provider }
     *     
     */
    public void setProvider(SecurityPolicy.Provider value) {
        this.provider = value;
    }

    /**
     * Obtient la valeur de la propriété domain.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDomain() {
        return domain;
    }

    /**
     * Définit la valeur de la propriété domain.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDomain(String value) {
        this.domain = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Consumer {

        @XmlAttribute(name = "credentialType")
        protected String credentialType;
        @XmlAttribute(name = "checkAuthN")
        protected Boolean checkAuthN;
        @XmlAttribute(name = "checkAuthZ")
        protected Boolean checkAuthZ;

        /**
         * Obtient la valeur de la propriété credentialType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCredentialType() {
            return credentialType;
        }

        /**
         * Définit la valeur de la propriété credentialType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCredentialType(String value) {
            this.credentialType = value;
        }

        /**
         * Obtient la valeur de la propriété checkAuthN.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isCheckAuthN() {
            return checkAuthN;
        }

        /**
         * Définit la valeur de la propriété checkAuthN.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setCheckAuthN(Boolean value) {
            this.checkAuthN = value;
        }

        /**
         * Obtient la valeur de la propriété checkAuthZ.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isCheckAuthZ() {
            return checkAuthZ;
        }

        /**
         * Définit la valeur de la propriété checkAuthZ.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setCheckAuthZ(Boolean value) {
            this.checkAuthZ = value;
        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "credential"
    })
    public static class Provider {

        @XmlElement(name = "Credential", required = true)
        protected SecurityPolicy.Provider.Credential credential;
        @XmlAttribute(name = "SFtype")
        protected String sFtype;
        @XmlAttribute(name = "serviceName")
        protected String serviceName;
        @XmlAttribute(name = "ressourceURL")
        protected String ressourceURL;

        /**
         * Obtient la valeur de la propriété credential.
         * 
         * @return
         *     possible object is
         *     {@link SecurityPolicy.Provider.Credential }
         *     
         */
        public SecurityPolicy.Provider.Credential getCredential() {
            return credential;
        }

        /**
         * Définit la valeur de la propriété credential.
         * 
         * @param value
         *     allowed object is
         *     {@link SecurityPolicy.Provider.Credential }
         *     
         */
        public void setCredential(SecurityPolicy.Provider.Credential value) {
            this.credential = value;
        }

        /**
         * Obtient la valeur de la propriété sFtype.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSFtype() {
            return sFtype;
        }

        /**
         * Définit la valeur de la propriété sFtype.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSFtype(String value) {
            this.sFtype = value;
        }

        /**
         * Obtient la valeur de la propriété serviceName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getServiceName() {
            return serviceName;
        }

        /**
         * Définit la valeur de la propriété serviceName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setServiceName(String value) {
            this.serviceName = value;
        }

        /**
         * Obtient la valeur de la propriété ressourceURL.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRessourceURL() {
            return ressourceURL;
        }

        /**
         * Définit la valeur de la propriété ressourceURL.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRessourceURL(String value) {
            this.ressourceURL = value;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         * 
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         * 
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class Credential {

            @XmlAttribute(name = "authNTypeForwarded")
            protected String authNTypeForwarded;
            @XmlAttribute(name = "credentialType")
            protected String credentialType;
            @XmlAttribute(name = "genericAccount")
            protected String genericAccount;
            @XmlAttribute(name = "genericCredential")
            protected String genericCredential;
            @XmlAttribute(name = "authNElement")
            protected String authNElement;

            /**
             * Obtient la valeur de la propriété authNTypeForwarded.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAuthNTypeForwarded() {
                return authNTypeForwarded;
            }

            /**
             * Définit la valeur de la propriété authNTypeForwarded.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAuthNTypeForwarded(String value) {
                this.authNTypeForwarded = value;
            }

            /**
             * Obtient la valeur de la propriété credentialType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCredentialType() {
                return credentialType;
            }

            /**
             * Définit la valeur de la propriété credentialType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCredentialType(String value) {
                this.credentialType = value;
            }

            /**
             * Obtient la valeur de la propriété genericAccount.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGenericAccount() {
                return genericAccount;
            }

            /**
             * Définit la valeur de la propriété genericAccount.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGenericAccount(String value) {
                this.genericAccount = value;
            }

            /**
             * Obtient la valeur de la propriété genericCredential.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGenericCredential() {
                return genericCredential;
            }

            /**
             * Définit la valeur de la propriété genericCredential.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGenericCredential(String value) {
                this.genericCredential = value;
            }

            /**
             * Obtient la valeur de la propriété authNElement.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAuthNElement() {
                return authNElement;
            }

            /**
             * Définit la valeur de la propriété authNElement.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAuthNElement(String value) {
                this.authNElement = value;
            }

        }

    }

}
