/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.transformations.sfgenerator.impl;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.io.FileUtils;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.transformations.sfgenerator.SFGeneratorException;
import eu.chorevolution.transformations.sfgenerator.SFType;
import eu.chorevolution.transformations.sfgenerator.impl.utility.Encryptor;
import eu.chorevolution.transformations.sfgenerator.impl.utility.Utilities;
import eu.chorevolution.transformations.sfgenerator.impl.utility.XACMLGeneration;
import eu.chorevolution.transformations.sfgenerator.model.SF;
import eu.chorevolution.transformations.sfgenerator.util.SecurityModelUtil;
import eu.chorevolution.modelingnotations.configuration.ObjectFactory;
import eu.chorevolution.modelingnotations.configuration.SecurityPolicy;
import eu.chorevolution.modelingnotations.security.Authentication;
import eu.chorevolution.modelingnotations.security.AuthenticationTypeForwarded;
import eu.chorevolution.modelingnotations.security.CredentialType;
import eu.chorevolution.modelingnotations.security.SecurityModel;
import eu.chorevolution.transformations.sfgenerator.ConnectionAccount;
import eu.chorevolution.transformations.sfgenerator.LoginPasswordConnectionAccount;
import eu.chorevolution.transformations.sfgenerator.SFGenerator;

public class SFGeneratorImpl implements SFGenerator {

	private static final Logger logger = LoggerFactory.getLogger(SFGeneratorImpl.class);


	private File generateConfigurationFile(String sfName, String destDir, byte[] securityModel, List<String> groups, ConnectionAccount account, SFType securityFilterType, String domain) throws SFGeneratorException {



		ObjectFactory SecurityPolicyFactory = new ObjectFactory();
		SecurityPolicy securityPolicy = SecurityPolicyFactory.createSecurityPolicy();


		securityPolicy.setConsumer(SecurityPolicyFactory.createSecurityPolicyConsumer());
		securityPolicy.getConsumer().setCheckAuthN(true);
		securityPolicy.getConsumer().setCheckAuthZ(true);
		securityPolicy.getConsumer().setCredentialType("User");

		securityPolicy.setProvider(SecurityPolicyFactory.createSecurityPolicyProvider());
		//		securityPolicy.getProvider().setServiceName(sModel.getSecuritypolicyset().getServiceName());
		//		securityPolicy.getProvider().setRessourceURL(sModel.getSecuritypolicyset().getRessourceURL());

		securityPolicy.getProvider().setSFtype(securityFilterType.name());
		securityPolicy.setDomain(domain);
		if (securityFilterType.equals(securityFilterType.ADAPTATION))			
		{
			File securityFile = Utilities.createSecurityModel(destDir, sfName, securityModel);
			URI securityURI = URI.createURI(securityFile.toURI().toString());
			SecurityModel sModel = SecurityModelUtil.loadSecurityModel(securityURI);

			if (sModel.getSecuritypolicyset()==null)
				throw new SFGeneratorException("No security policy set defined into the security model");


			securityPolicy.getProvider().setServiceName(sModel.getSecuritypolicyset().getServiceName());
			if (sModel.getSecuritypolicyset().getAuthentication()!=null){

				EList<Authentication> AuthNList = sModel.getSecuritypolicyset().getAuthentication();
				for (Iterator iterator = AuthNList.iterator(); iterator.hasNext();) {
					Authentication authentication = (Authentication) iterator.next();
					if (authentication.getCredentialType().equals(CredentialType.USERNAME_PASSWORD)){

						// todo check if the credential is coming from the security model, or the designer or null
						eu.chorevolution.modelingnotations.configuration.SecurityPolicy.Provider.Credential e = SecurityPolicyFactory.createSecurityPolicyProviderCredential();
						e.setAuthNTypeForwarded(authentication.getAuthNTypeForwarded().getName());		



						if (authentication.getAuthNTypeForwarded().equals(AuthenticationTypeForwarded.GENERIC_ACCOUNT)){
							Encryptor enc = new Encryptor("dsadsadasa4444");
							if (account!=null)
							{
								if (account instanceof LoginPasswordConnectionAccount) {
									LoginPasswordConnectionAccount lpAccount = (LoginPasswordConnectionAccount) account;

									e.setCredentialType(authentication.getCredentialType().getName());
									if (lpAccount!=null){
										e.setGenericAccount(lpAccount.getLogin());
										try {
											e.setGenericCredential(enc.encode(lpAccount.getPassword()));
										} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
												| IllegalBlockSizeException | BadPaddingException
												| UnsupportedEncodingException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
											throw new SFGeneratorException("Password encryption error");
										}
									}
									e.setAuthNElement(null);
								}
								else{
									throw new SFGeneratorException("Authentication mechanism not supported");
								}
							} else if (authentication.getGenericAccount()!=null && authentication.getGenericCredential()!=null) {
								e.setCredentialType(authentication.getCredentialType().getName());
								e.setGenericAccount(authentication.getGenericAccount());
								try {
									e.setGenericCredential(enc.encode(authentication.getGenericCredential()));
								} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
										| IllegalBlockSizeException | BadPaddingException
										| UnsupportedEncodingException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
									throw new SFGeneratorException("Password encryption error");
								}
								e.setAuthNElement(null);
							} else {
								throw new SFGeneratorException("Generic account required but not provided");
							}
						}
						else{
							e.setCredentialType(authentication.getCredentialType().getName());
							e.setAuthNElement(authentication.getAuthNElement().getName());
						}
						securityPolicy.getProvider().setCredential(e);
					}
				}
			}
		}

		File configxml = new File(destDir+System.getProperty("file.separator")+"WEB-INF"+System.getProperty("file.separator")+"config.xml");
		configxml.getParentFile().mkdirs();

		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(SecurityPolicy.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(securityPolicy, configxml);
		} catch (JAXBException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			throw new SFGeneratorException(e1);
		}
		return configxml;
	}

	//Generation of the security filter present in front of the legacy service with a custom implementation
	@Override
	public SF generateSecurityFilter(String sfName, String STSUrl, String domain, byte[] securityModel, List<String> groups, ConnectionAccount account, byte[] adaptationJarFile)
			throws SFGeneratorException {
		SF sf = new SF(sfName);
		try {
			String destDir = FileUtils.getTempDirectoryPath();
			destDir = Utilities.getDestinationFolderPath(destDir, sfName);
			Utilities.deleteProjectFolder(destDir);
			
			File securityFile = Utilities.createSecurityModel(destDir, sfName, securityModel);
			URI securityURI = URI.createURI(securityFile.toURI().toString());
			SecurityModel sModel = SecurityModelUtil.loadSecurityModel(securityURI);
			
			
			if (sModel.getSecuritypolicyset()==null)
				throw new SFGeneratorException("No security policy set defined into the security model");

			if (sModel.getSecuritypolicyset().getAuthentication()!=null){

				EList<Authentication> AuthNList = sModel.getSecuritypolicyset().getAuthentication();
				for (Iterator iterator = AuthNList.iterator(); iterator.hasNext();) {
					Authentication authentication = (Authentication) iterator.next();
					if (!authentication.getAuthNTypeForwarded().equals(AuthenticationTypeForwarded.CUSTOM_ACCOUNT))
						throw new SFGeneratorException("The security Model is not in line with the method used");
				}
			}
			else 
			{
				throw new SFGeneratorException("No authentication policy defined into the security model");
			}
			
			
			File warResultFile = Utilities.copyWarTemplate(destDir);
			File configurationFile = generateConfigurationFile(sfName, destDir, null, null, null, SFType.CUSTOM_ADAPTATION, null);
			Utilities.addConfigFileintoWar(destDir, configurationFile);
			File webXml = Utilities.createWebXml(destDir, sf.getName(), "");
			Utilities.addWebXmlFileintoWar(destDir, webXml);
			Utilities.addZipFileintoWar(destDir, adaptationJarFile);
			sf.setWar(Utilities.getBytesFromWar(warResultFile));
			Utilities.deleteProjectFolder(destDir);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sf;

	}

	//Generation of the security filter present in front of the choreography service with a custom implementation
	@Override
	public SF generateSecurityFilter(String sfName, String STSUrl, String domain, List<String> groups, byte[] protectionJarFile)
			throws SFGeneratorException {
		SF sf = new SF(sfName);
		try {
			String destDir = FileUtils.getTempDirectoryPath();
			destDir = Utilities.getDestinationFolderPath(destDir, sfName);
			Utilities.deleteProjectFolder(destDir);
			File warResultFile = Utilities.copyWarTemplate(destDir);
			File configurationFile = generateConfigurationFile(sfName, destDir, null, null, null, SFType.CUSTOM_PROTECTION, null);
			Utilities.addConfigFileintoWar(destDir, configurationFile);
			File webXml = Utilities.createWebXml(destDir, sf.getName(), "");
			Utilities.addWebXmlFileintoWar(destDir, webXml);
			Utilities.addZipFileintoWar(destDir, protectionJarFile);
			sf.setWar(Utilities.getBytesFromWar(warResultFile));
			Utilities.deleteProjectFolder(destDir);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sf;
	}

	//Generation of the security filter present  in front of the legacy service
	@Override
	public SF generateSecurityFilter(String sfName, String STSUrl, String domain, byte[] securityModel, List<String> groups, ConnectionAccount account) throws SFGeneratorException {

		SF sf = new SF(sfName);
		try {



			String destDir = FileUtils.getTempDirectoryPath();
			String initialDestDir = destDir;
			destDir = Utilities.getDestinationFolderPath(destDir, sfName);
			Utilities.deleteProjectFolder(destDir);
			File warResultFile = Utilities.copyWarTemplate(destDir);
			File configurationFile = generateConfigurationFile(sfName, destDir, securityModel, groups, account, SFType.ADAPTATION, domain);
			Utilities.addConfigFileintoWar(destDir, configurationFile);
			File webXml = Utilities.createWebXml(destDir, sf.getName(), STSUrl);
			Utilities.addWebXmlFileintoWar(destDir, webXml);

			File XACMLPolicies = XACMLGeneration.createXACMLFile(destDir, groups);
			if (XACMLPolicies!=null)
			{
				Utilities.addXACMLPoliciesFileintoWar(destDir, XACMLPolicies);
			}
			sf.setWar(Utilities.getBytesFromWar(warResultFile));
			Utilities.deleteProjectFolder(destDir);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sf;
	}

	//Generation of the security filter present in front of the choreography
	@Override
	public SF generateSecurityFilter(String sfName, String STSUrl, String domain, List<String> groups)
			throws SFGeneratorException {
		SF sf = new SF(sfName);
		try {
			String destDir = FileUtils.getTempDirectoryPath();
			destDir = Utilities.getDestinationFolderPath(destDir, sfName);
			Utilities.deleteProjectFolder(destDir);
			File warResultFile = Utilities.copyWarTemplate(destDir);
			File configurationFile = generateConfigurationFile(sfName, destDir, null, groups, null, SFType.PROTECTION, domain);
			Utilities.addConfigFileintoWar(destDir, configurationFile);
			File webXml = Utilities.createWebXml(destDir, sf.getName(), STSUrl);
			Utilities.addWebXmlFileintoWar(destDir, webXml);
			File XACMLPolicies = XACMLGeneration.createXACMLFile(destDir, groups);
			if (XACMLPolicies!=null){
				Utilities.addXACMLPoliciesFileintoWar(destDir, XACMLPolicies);
			}
			sf.setWar(Utilities.getBytesFromWar(warResultFile));
			Utilities.deleteProjectFolder(destDir);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sf;
	}


	public static void main(String[] args) throws IOException {
		List<String> groups = new ArrayList<String>();
		groups.add("group1");
		groups.add("group5");
		String domain = "Master";


		{
			System.out.println("Generation of a custom SF in front of the choreography : testCustomProtection");
			String SFName = "testCustomProtection";

			SFGenerator cdGenerator = new SFGeneratorImpl();
			File jar = new File( "." + File.separatorChar + "sf-protection-authN-1.0.0-default.zip" );
			byte[] jarFileArray = FileUtils.readFileToByteArray(jar);

			//Generation of the security filter present in front of the choreography service with a custom implementation
			//public SF generateSecurityFilter(String sfName, String STSUrl, String domain, List<String> groups, byte[] protectionJarFile)
			SF sf = cdGenerator.generateSecurityFilter(SFName, "http://127.0.0.1:8080/SecurityTokenService", domain, groups, jarFileArray);
			FileUtils.writeByteArrayToFile(new File( "." + File.separatorChar + SFName + File.separatorChar + "SecurityfilterServletProxy.war"), sf.getWar());
		}

		{
			System.out.println("Generation of a custom SF with google recaptcha in front of the choreography : testRecaptchaProtection");
			String SFName = "testRecaptchaProtection";

			SFGenerator cdGenerator = new SFGeneratorImpl();
			File jar = new File( "." + File.separatorChar + "sf-recaptcha-protection-1.0.0-default.zip" );
			byte[] jarFileArray = FileUtils.readFileToByteArray(jar);

			//Generation of the security filter present in front of the choreography service with a custom implementation
			//public SF generateSecurityFilter(String sfName, String STSUrl, String domain, List<String> groups, byte[] protectionJarFile)
			SF sf = cdGenerator.generateSecurityFilter(SFName, "http://127.0.0.1:8080/SecurityTokenService", domain, groups, jarFileArray);
			FileUtils.writeByteArrayToFile(new File( "." + File.separatorChar + SFName + File.separatorChar + "SecurityfilterServletProxy.war"), sf.getWar());
		}

		{
			System.out.println("Generation of a custom SF in front of a service : testCustomAdaptation");
			String SFName = "testCustomAdaptation";

			SFGenerator cdGenerator = new SFGeneratorImpl();
			File jar = new File( "." + File.separatorChar + "sf-adaptation-authN-1.0.0-default.zip" );
			byte[] jarFileArray = FileUtils.readFileToByteArray(jar);
			Path securityPath = Paths.get("SecModelCustomAdaptation.security");
			byte[] securityModel = Files.readAllBytes(securityPath);
			
			//Generation of the security filter present in front of the legacy service with a custom implementation
			//public SF generateSecurityFilter(String sfName, String STSUrl, String domain, byte[] securityModel, List<String> groups, ConnectionAccount account, byte[] adaptationJarFile)

			SF sf = cdGenerator.generateSecurityFilter(SFName, "http://127.0.0.1:8080/SecurityTokenService", domain ,securityModel, groups, null, jarFileArray);
			FileUtils.writeByteArrayToFile(new File( "." + File.separatorChar + SFName + File.separatorChar + "SecurityfilterServletProxy.war"), sf.getWar());
		}




		//		{
		//			System.out.println("Generation of a SF in front of the choreography");
		//			String SFName = "testProtection";
		//
		//			SFGenerator cdGenerator = new SFGeneratorImpl();
		//
		//
		//			
		//			SF cd = cdGenerator.generateSecurityFilter(SFName, "http://127.0.0.1:8080/SecurityTokenService", domain , groups);
		//			FileUtils.writeByteArrayToFile(new File( "." + File.separatorChar + SFName + File.separatorChar + "SecurityfilterServletProxy.war"), cd.getWar());
		//		}
		//
		//		{
		//			System.out.println("Generation of a SF in front of a legacy Service. During the service definition, the service owner has provided an account to access it");
		//			String SFName = "testGenAccountWithCred";
		//			Path securityPath = Paths.get("SecModelGenAccountWithCred.security");
		//			byte[] securityModel = Files.readAllBytes(securityPath);
		//
		//			SFGenerator cdGenerator = new SFGeneratorImpl();		
		//			SF cd = cdGenerator.generateSecurityFilter(SFName, "http://127.0.0.1:8080/SecurityTokenService", domain , securityModel, groups, null);
		//			FileUtils.writeByteArrayToFile(new File( "." + File.separatorChar + SFName + File.separatorChar + "SecurityfilterServletProxy.war"), cd.getWar());
		//		}
		//
		//		{
		//			System.out.println("Generation of a SF in front of a legacy Service. During the service definition, the service owner has not provided an account to access it.  the account must be provided by the designer");
		//			String SFName = "testGenAccountWithoutCred";
		//			Path securityPath = Paths.get("SecModelGenAccountWithoutCred.security");
		//			byte[] securityModel = Files.readAllBytes(securityPath);
		//
		//			SFGenerator cdGenerator = new SFGeneratorImpl();
		//			LoginPasswordConnectionAccount account = new LoginPasswordConnectionAccount();
		//			account.setLogin("root");
		//			account.setPassword("password");
		//
		//			SF cd = cdGenerator.generateSecurityFilter(SFName, "http://127.0.0.1:8080/SecurityTokenService", domain , securityModel, groups, account);
		//			FileUtils.writeByteArrayToFile(new File( "." + File.separatorChar + SFName + File.separatorChar + "SecurityfilterServletProxy.war"), cd.getWar());
		//		}
		//		groups = null;
		//		{
		//			
		//			System.out.println("Generation of a SF in front of a legacy Service. During the service definition, the service owner has required a nominative user account");
		//			String SFName = "testUserAccount";
		//			Path securityPath = Paths.get("SecModelUserAccount.security");
		//			byte[] securityModel = Files.readAllBytes(securityPath);
		//
		//			SFGenerator cdGenerator = new SFGeneratorImpl();
		//			SF cd = cdGenerator.generateSecurityFilter(SFName, "http://127.0.0.1:8080/SecurityTokenService", domain , securityModel, groups, null);
		//			FileUtils.writeByteArrayToFile(new File( "." + File.separatorChar + SFName + File.separatorChar + "SecurityfilterServletProxy.war"), cd.getWar());
		//		}
		//
		//		
		//		// usecase generation
		//		List<String> groups2 = new ArrayList<String>();
		//		groups2.add("ServiceProviderApprover");
		//		{
		//			System.out.println("Generation of a SF in front of the choreography");
		//			String SFName = "sfSTApp";
		//
		//			SFGenerator cdGenerator = new SFGeneratorImpl();
		//
		//
		//			
		//			SF cd = cdGenerator.generateSecurityFilter(SFName, "http://127.0.0.1:8080/SecurityTokenService", domain , groups2);
		//			FileUtils.writeByteArrayToFile(new File( "." + File.separatorChar + "sfSTApp.war"), cd.getWar());
		//		}
		//		{
		//			System.out.println("Generation of a SF in front of a Parking Service.");
		//			String SFName = "sfOSMParking";
		//			Path securityPath = Paths.get("ParkingService.security");
		//			byte[] securityModel = Files.readAllBytes(securityPath);
		//
		//			SFGenerator cdGenerator = new SFGeneratorImpl();
		//			SF cd = cdGenerator.generateSecurityFilter(SFName, "http://127.0.0.1:8080/SecurityTokenService", domain , securityModel, groups2, null);
		//			FileUtils.writeByteArrayToFile(new File( "." + File.separatorChar + "sfOSMParking.war"), cd.getWar());
		//		}
		//
		//		{
		//			System.out.println("Generation of a SF in front of a POI Service.");
		//			String SFName = "sfPoi";
		//			Path securityPath = Paths.get("POIService.security");
		//			byte[] securityModel = Files.readAllBytes(securityPath);
		//			LoginPasswordConnectionAccount account = new LoginPasswordConnectionAccount();
		//			account.setLogin("test");
		//			account.setPassword("password");
		//			SFGenerator cdGenerator = new SFGeneratorImpl();
		//			SF cd = cdGenerator.generateSecurityFilter(SFName, "http://127.0.0.1:8080/SecurityTokenService", domain , securityModel, groups2, account);
		//			FileUtils.writeByteArrayToFile(new File( "." + File.separatorChar + "sfPoi.war"), cd.getWar());
		//		}
		//
		//		{
		//			System.out.println("Generation of a SF in front of a Traffic Information Service.");
		//			String SFName = "sfTrafficInformation";
		//			Path securityPath = Paths.get("TrafficInformationService.security");
		//			byte[] securityModel = Files.readAllBytes(securityPath);
		//			LoginPasswordConnectionAccount account = new LoginPasswordConnectionAccount();
		//			account.setLogin("test");
		//			account.setPassword("password");
		//			SFGenerator cdGenerator = new SFGeneratorImpl();
		//			SF cd = cdGenerator.generateSecurityFilter(SFName, "http://127.0.0.1:8080/SecurityTokenService", domain , securityModel, groups2, account);
		//			FileUtils.writeByteArrayToFile(new File( "." + File.separatorChar + "sfTrafficInformation.war"), cd.getWar());
		//		}
		//
		//		{
		//			System.out.println("Generation of a SF in front of a JourneyPlanner Service.");
		//			String SFName = "sfJourneyPlanner";
		//			Path securityPath = Paths.get("JourneyPlannerService.security");
		//			byte[] securityModel = Files.readAllBytes(securityPath);
		//			LoginPasswordConnectionAccount account = new LoginPasswordConnectionAccount();
		//			account.setLogin("test");
		//			account.setPassword("password");
		//			SFGenerator cdGenerator = new SFGeneratorImpl();
		//			SF cd = cdGenerator.generateSecurityFilter(SFName, "http://127.0.0.1:8080/SecurityTokenService", domain , securityModel, groups2, account);
		//			FileUtils.writeByteArrayToFile(new File( "." + File.separatorChar + "sfJourneyPlanner.war"), cd.getWar());
		//		}

	}




}
