/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.transformations.sfgenerator.impl.utility;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang3.ArrayUtils;

public class Encryptor {

	private static String algo ="AES";
	private SecretKeySpec keySpec = null;


	public Encryptor(String secretKey) {
		
		
		//System.out.println("CREATE ENCRYPTOR WITH : " + secretKey);
        String actualKey = secretKey;
        if (actualKey.length() < 16) {
            StringBuilder actualKeyPadding = new StringBuilder(actualKey);
            for (int i = 0; i < 16 - actualKey.length(); i++) {
                actualKeyPadding.append('0');
            }
            actualKey = actualKeyPadding.toString();
        }
        

		try {
			keySpec = new SecretKeySpec(ArrayUtils.subarray(
			        actualKey.getBytes("UTF-8"), 0, 16), algo);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}




	public String encode(final String value) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException{
		String encodedValue = null;
		if (value != null) {
			final Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, keySpec);
			byte[] envVal = cipher.doFinal(value.getBytes("UTF-8"));
			encodedValue = Base64.getEncoder().encodeToString(envVal);
		}
		return encodedValue;
	}

	public String decode(final String encodedValue) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		//System.out.println("encoded value " + encodedValue);
		String value = null;
		if (encodedValue != null){
			final Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, keySpec);
			byte[] decodedValue  = Base64.getDecoder().decode(encodedValue);
			byte[] decVal = cipher.doFinal(decodedValue);
			value = new String(decVal, "UTF-8");
		}
		return value;
	}
	
	
	public static void main(String[] args) {
		String password = "password";
		Encryptor encryptor = new Encryptor("skjajsjsjljkdsa6633");
		try {
			String encryptedText = encryptor.encode(password);
			String decryptedText = encryptor.decode(encryptedText);
			
			System.out.println("original "  + password);
			System.out.println("encrypted "  + encryptedText);
			System.out.println("decrypted "  + decryptedText);
			
			
			System.out.println(encryptor.decode("AEdnzs++tE4XgTQVE6csCw=="));
			
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
