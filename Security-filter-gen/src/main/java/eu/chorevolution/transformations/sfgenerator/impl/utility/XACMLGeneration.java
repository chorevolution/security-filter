/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.transformations.sfgenerator.impl.utility;

import java.io.File;
import java.io.Serializable;
import java.io.StringWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.text.html.HTMLDocument.Iterator;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import org.apache.commons.io.FileUtils;

import oasis.names.tc.xacml._3_0.core.schema.wd_17.AdviceExpressions;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.AllOf;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.AnyOf;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.Attribute;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.AttributeDesignatorType;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.AttributeSelectorType;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.AttributeValueType;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.Condition;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.Content;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.DefaultsType;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.EffectType;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.Match;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.ObjectFactory;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.ObligationExpressions;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.Policy;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.PolicyIssuer;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.PolicySet;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.Rule;
import oasis.names.tc.xacml._3_0.core.schema.wd_17.Target;;

public class XACMLGeneration {
	public static void main(String[] args) {
		List<String> groups = new ArrayList<String>();
		groups.add("group1");
		groups.add("group2");
		groups.add("group3");
		String destDir = FileUtils.getTempDirectoryPath();
		XACMLGeneration.createXACMLFile(destDir, groups);
	}




	public static File createXACMLFile(String destDir, List<String> groups){
		File XACMLPolicies = null;
		if (groups!=null){

			XACMLPolicies = new File(destDir+System.getProperty("file.separator")+"WEB-INF"+System.getProperty("file.separator")+"policy.xml");;
			String description = "description";

			PolicyIssuer policyIssuer = null;
			Target target = new Target(null);
			ObligationExpressions obligationExpressions = null;
			AdviceExpressions adviceExpressions = null;
			String policyId = "policyID";
			String version = "2.0";
			String ruleCombiningAlgId = "urn:oasis:names:tc:xacml:3.0:rule-combining-algorithm:deny-unless-permit";

			DefaultsType policyDefaults = null;
			BigInteger maxDelegationDepth = null;
			List<Serializable> combinerParametersAndRuleCombinerParametersAndVariableDefinitions = new ArrayList<Serializable>();
			Condition condition = null;
			String ruleId = "ruleID";
			EffectType effect = EffectType.PERMIT;
			List<AnyOf> anyOves = new ArrayList<AnyOf>();

			List<AllOf> allOves = new ArrayList<AllOf>();


			for (java.util.Iterator iterator = groups.iterator(); iterator.hasNext();) {
				String group = (String) iterator.next();

				List<Match> matches = new ArrayList<Match>();
				String matchId = "urn:oasis:names:tc:xacml:1.0:function:string-equal";
				String category = "urn:oasis:names:tc:xacml:1.0:subject-category:access-subject";
				String attributeId = "group";
				String dataType = "http://www.w3.org/2001/XMLSchema#string";
				String issuer = null;
				boolean mustBePresent  = false;
				AttributeDesignatorType attributeDesignator = new AttributeDesignatorType(category, attributeId, dataType, issuer, mustBePresent);
				AttributeSelectorType attributeSelector = null;
				Map<QName, String> otherAttributes = new HashMap<QName, String>();
				List<Serializable> content = new ArrayList<Serializable>();
				content.add(group);
				AttributeValueType attributeValue = new AttributeValueType(content, dataType, otherAttributes);
				Match match = new Match(attributeValue, attributeSelector, attributeDesignator, matchId);
				matches.add(match );
				AllOf e2 = new AllOf(matches);
				allOves.add(e2);


			}
			AnyOf e = new AnyOf(allOves);
			anyOves.add(e);

			Target targetrule = new Target(anyOves);
			Rule rule1 = new Rule(description, targetrule, condition, obligationExpressions, adviceExpressions, ruleId, effect);
			combinerParametersAndRuleCombinerParametersAndVariableDefinitions.add(rule1);
			Policy policy = new Policy(description, 
					policyIssuer, 
					policyDefaults, 
					target, 
					combinerParametersAndRuleCombinerParametersAndVariableDefinitions, 
					obligationExpressions, 
					adviceExpressions, 
					policyId, 
					version, 
					ruleCombiningAlgId, 
					maxDelegationDepth);


			ObjectFactory xacmlFactory = new ObjectFactory();

			try {
				StringWriter writer = new StringWriter();
				JAXBContext context;
				context = JAXBContext.newInstance(PolicySet.class);
				Marshaller m = context.createMarshaller();
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				m.marshal(policy, XACMLPolicies);

				//			String theXML = writer.toString();
				//			System.out.println(theXML);
			} catch (JAXBException ex) {
				// TODO Auto-generated catch block
				ex.printStackTrace();
			}
		}
		return XACMLPolicies;
	}

}
