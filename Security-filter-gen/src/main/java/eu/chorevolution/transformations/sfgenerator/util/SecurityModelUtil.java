/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.transformations.sfgenerator.util;
import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import eu.chorevolution.modelingnotations.security.SecurityModel;
import eu.chorevolution.modelingnotations.security.impl.SecurityPackageImpl;
import eu.chorevolution.transformations.sfgenerator.SFGeneratorException;

public class SecurityModelUtil {

	
	
	/**
	 * load the security model
	 * @param securityURI the model file URI
	 * @return The security model
	 * @throws SFGeneratorException
	 */
	public static SecurityModel loadSecurityModel(URI securityURI) throws SFGeneratorException {
		SecurityPackageImpl.init();

		Resource resource = new XMIResourceFactoryImpl().createResource(securityURI);

		try {
			// load the resource
			resource.load(null);

		} catch (IOException e) {
			throw new SFGeneratorException("Error to load the resource: " + resource.getURI().toFileString());
		}

		SecurityModel securityModel = (SecurityModel) resource.getContents().get(0);

		if (securityModel == null || securityModel.getSecuritypolicyset() == null) {
			throw new SFGeneratorException(
					"None security policy founded in the model: " + resource.getURI().toFileString());
		}

		return securityModel;
	}
}
