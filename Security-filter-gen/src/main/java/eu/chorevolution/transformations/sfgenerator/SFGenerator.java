/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.transformations.sfgenerator;

import java.util.List;

import eu.chorevolution.transformations.sfgenerator.model.SF;

public interface SFGenerator {
	
	/**
	 * Generation of the security filter present  in front of the legacy service
	 * @param sfName Name of the securityFilter
	 * @param STSUrl URL of the Federation Server
	 * @param domain 
	 * @param securityModel The security model of the service
	 * @param groups The list of groups allow to access the service
	 * @param account The account provided by the choreography designer in case of the model required generic account (Null if the model required user account)
	 * @return A SF element which contains the WAR element
	 * @throws SFGeneratorException
	 */
	SF generateSecurityFilter(String sfName, String STSUrl, String domain, byte[] securityModel, List<String> groups, ConnectionAccount account) throws SFGeneratorException;

	/**
	 * Generation of the security filter present in front of the choreography
	 * @param sfName Name of the securityFilter
	 * @param STSUrl URL of the Federation Server
	 * @param domain 
	 * @return A SF element which contains the WAR element
	 * @throws SFGeneratorException
	 */
	SF generateSecurityFilter(String sfName, String STSUrl, String domain, List<String> groups) throws SFGeneratorException;

	/**
	 * Generation of the security filter present in front of the legacy service with a custom implementation
	 * @param sfName Name of the securityFilter
	 * @param STSUrl URL of the Federation Server
	 * @param domain 
	 * @param securityModel The security model of the service
	 * @param groups The list of groups allow to access the service
	 * @param account The account provided by the choreography designer in case of the model required generic account (Null if the model required user account)
	 * @param adaptationJarFile jar used to realise the adaptation of the security
	 * @return A SF element which contains the WAR element
	 * @throws SFGeneratorException
	 */
	SF generateSecurityFilter(String sfName, String STSUrl, String domain, byte[] securityModel, List<String> groups, ConnectionAccount account, byte[] adaptationJarFile) throws SFGeneratorException;

	/**
	 * Generation of the security filter present in front of the choreography service with a custom implementation
	 * @param sfName Name of the securityFilter
	 * @param STSUrl URL of the Federation Server
	 * @param domain 
	 * @param protectionJarFile jar used to realise the adaptation of the security
	 * @return A SF element which contains the WAR element
	 * @throws SFGeneratorException
	 */
	SF generateSecurityFilter(String sfName, String STSUrl, String domain, List<String> groups, byte[] protectionJarFile) throws SFGeneratorException;


}
