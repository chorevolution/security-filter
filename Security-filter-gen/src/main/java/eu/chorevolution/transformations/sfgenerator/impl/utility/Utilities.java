/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.transformations.sfgenerator.impl.utility;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import eu.chorevolution.transformations.sfgenerator.SFGeneratorException;

public class Utilities {

	private static final int BUFFER_SIZE = 4096;
	private final static int BUFFERSIZE = 32768; 
	public final static String FILESEPARATOR = System.getProperty("file.separator"); 
	public final static String WARTEMPLATENAME =  "SecurityfilterServletProxy.war";

	private static Logger logger = LoggerFactory.getLogger(Utilities.class);
	private final static String xmlLicenseHeader = new StringBuilder()
			.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append(System.getProperty("line.separator"))
			.append("<!--").append(System.getProperty("line.separator"))
			.append("Copyright 2015 The CHOReVOLUTION project").append(System.getProperty("line.separator"))
			.append("Licensed under the Apache License, Version 2.0 (the \"License\");").append(System.getProperty("line.separator"))
			.append("you may not use this file except in compliance with the License.").append(System.getProperty("line.separator"))
			.append("You may obtain a copy of the License at").append(System.getProperty("line.separator"))
			.append(System.getProperty("line.separator"))
			.append("\t http://www.apache.org/licenses/LICENSE-2.0").append(System.getProperty("line.separator"))
			.append(System.getProperty("line.separator"))
			.append("Unless required by applicable law or agreed to in writing, software").append(System.getProperty("line.separator"))
			.append("distributed under the License is distributed on an \"AS IS\" BASIS").append(System.getProperty("line.separator"))
			.append("WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.").append(System.getProperty("line.separator"))
			.append("See the License for the specific language governing permissions and ").append(System.getProperty("line.separator"))
			.append("limitations under the License.").append(System.getProperty("line.separator"))
			.append("-->").append(System.getProperty("line.separator"))
			.toString();

	private final static String propertiesLicenseHeader = new StringBuilder()
			.append("#").append(System.getProperty("line.separator"))
			.append("# Copyright 2015 The CHOReVOLUTION project").append(System.getProperty("line.separator"))
			.append(System.getProperty("line.separator"))
			.append("# Licensed under the Apache License, Version 2.0 (the \"License\");").append(System.getProperty("line.separator"))
			.append("# you may not use this file except in compliance with the License.").append(System.getProperty("line.separator"))
			.append("# You may obtain a copy of the License at").append(System.getProperty("line.separator"))
			.append(System.getProperty("line.separator"))
			.append("\t http://www.apache.org/licenses/LICENSE-2.0").append(System.getProperty("line.separator"))
			.append("#").append(System.getProperty("line.separator"))
			.append("# Unless required by applicable law or agreed to in writing, software").append(System.getProperty("line.separator"))
			.append("# distributed under the License is distributed on an \"AS IS\" BASIS,").append(System.getProperty("line.separator"))
			.append("# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.").append(System.getProperty("line.separator"))
			.append("# See the License for the specific language governing permissions and").append(System.getProperty("line.separator"))
			.append("# limitations under the License.").append(System.getProperty("line.separator"))
			.append("#").append(System.getProperty("line.separator"))												
			.toString();


	/**
	 * generate the desination folder
	 * @param destDir
	 * @param sfName 
	 * @return
	 */
	public static String getDestinationFolderPath(String destDir, String sfName){
		return (destDir+File.separatorChar+"sf"+sfName.replaceAll(" ", "")+"-"+System.currentTimeMillis()).replaceAll("\\s", "_");
	}

	/**
	 * Create the Web.xml file of the web archive
	 * @param projectDir The project directory
	 * @param sfName the name of the security filter 
	 * @param sTSUrl The URL of the Federation server
	 * @return the web.xml file
	 */
	public static File createWebXml(String projectDir,String sfName, String sTSUrl){

		File webxml = new File(projectDir+System.getProperty("file.separator")+"WEB-INF"+System.getProperty("file.separator")+"web.xml");
		String content = new StringBuilder(xmlLicenseHeader)

				//				.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append(System.getProperty("line.separator"))
				.append("<web-app version=\"2.5\" xmlns=\"http://java.sun.com/xml/ns/javee\"").append(System.getProperty("line.separator"))
				.append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"").append(System.getProperty("line.separator"))
				.append("xsi:schemaLocation=\"http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd\">").append(System.getProperty("line.separator"))

				.append("\t <display-name>"+sfName+"</display-name>").append(System.getProperty("line.separator"))

				.append("\t <filter>").append(System.getProperty("line.separator"))
				.append("\t\t <filter-name>SetInvocationAddressfilter</filter-name>").append(System.getProperty("line.separator"))
				.append("\t\t <display-name>SetInvocationAddressfilter</display-name>").append(System.getProperty("line.separator"))
				.append("\t\t <filter-class>eu.chorevolution.SetInvocationAddressFilter</filter-class>").append(System.getProperty("line.separator"))
				.append("\t </filter>").append(System.getProperty("line.separator"))

				.append("\t <filter>").append(System.getProperty("line.separator"))
				.append("\t\t <filter-name>Securityfilter</filter-name>").append(System.getProperty("line.separator"))
				.append("\t\t <display-name>Securityfilter</display-name>").append(System.getProperty("line.separator"))
				.append("\t\t <filter-class>eu.chorevolution.SecurityFilter</filter-class>").append(System.getProperty("line.separator"))
				.append("\t\t <init-param>").append(System.getProperty("line.separator"))
				.append("\t\t\t <param-name>STS-URL</param-name>").append(System.getProperty("line.separator"))
				.append("\t\t\t <param-value>"+sTSUrl+"</param-value>").append(System.getProperty("line.separator"))
				.append("\t\t </init-param>").append(System.getProperty("line.separator"))
				.append("\t </filter>").append(System.getProperty("line.separator"))

				.append("\t <filter-mapping>").append(System.getProperty("line.separator"))
				.append("\t\t <filter-name>SetInvocationAddressfilter</filter-name>").append(System.getProperty("line.separator"))
				.append("\t\t <url-pattern>/"+sfName+"</url-pattern>").append(System.getProperty("line.separator"))
				.append("\t </filter-mapping>").append(System.getProperty("line.separator"))
				.append("\t <filter-mapping>").append(System.getProperty("line.separator"))
				.append("\t\t <filter-name>Securityfilter</filter-name>").append(System.getProperty("line.separator"))
				.append("\t\t <url-pattern>/"+sfName+"</url-pattern>").append(System.getProperty("line.separator"))
				.append("\t </filter-mapping>").append(System.getProperty("line.separator"))

				.append("\t<servlet>").append(System.getProperty("line.separator"))
				.append("\t\t <servlet-name>"+sfName+"</servlet-name>").append(System.getProperty("line.separator"))
				.append("\t\t <display-name>"+sfName+"</display-name>").append(System.getProperty("line.separator"))
				.append("\t\t <servlet-class>eu.chorevolution.SecurityServlet</servlet-class>").append(System.getProperty("line.separator"))
				//				.append("\t\t <init-param>").append(System.getProperty("line.separator"))
				//				.append("\t\t\t <param-name>targetUri</param-name>").append(System.getProperty("line.separator"))
				//				.append("\t\t\t <param-value>http://localhost:8080/testWsCxf/cxf/helloworld</param-value>").append(System.getProperty("line.separator"))
				//				.append("\t\t </init-param>").append(System.getProperty("line.separator"))
				.append("\t\t <init-param>").append(System.getProperty("line.separator"))
				.append("\t\t\t <param-name>log</param-name>").append(System.getProperty("line.separator"))
				.append("\t\t\t <param-value>true</param-value>").append(System.getProperty("line.separator"))
				.append("\t\t </init-param>").append(System.getProperty("line.separator"))
				.append("\t </servlet>").append(System.getProperty("line.separator"))

				.append("\t <listener>").append(System.getProperty("line.separator"))
				.append("\t\t <listener-class>org.springframework.web.context.ContextLoaderListener").append(System.getProperty("line.separator"))
				.append("\t\t </listener-class>").append(System.getProperty("line.separator"))
				.append("\t </listener>").append(System.getProperty("line.separator"))

				.append("\t <context-param>").append(System.getProperty("line.separator"))
				.append("\t\t <param-name>contextConfigLocation</param-name>").append(System.getProperty("line.separator"))
				.append("\t\t <param-value>WEB-INF/rest-servlet.xml</param-value>").append(System.getProperty("line.separator"))
				.append("\t </context-param>").append(System.getProperty("line.separator"))

				.append("\t <servlet>").append(System.getProperty("line.separator"))
				.append("\t\t <servlet-name>CXFServlet</servlet-name>").append(System.getProperty("line.separator"))
				.append("\t\t <servlet-class>org.apache.cxf.transport.servlet.CXFServlet").append(System.getProperty("line.separator"))
				.append("\t\t </servlet-class>").append(System.getProperty("line.separator"))
				.append("\t </servlet>").append(System.getProperty("line.separator"))

				.append("\t <servlet-mapping>").append(System.getProperty("line.separator"))
				.append("\t\t <servlet-name>"+sfName+"</servlet-name>").append(System.getProperty("line.separator"))
				.append("\t\t <url-pattern>/"+sfName+"</url-pattern>").append(System.getProperty("line.separator"))
				.append("\t </servlet-mapping>").append(System.getProperty("line.separator"))

				.append("\t <servlet-mapping>").append(System.getProperty("line.separator"))
				.append("\t\t <servlet-name>CXFServlet</servlet-name>").append(System.getProperty("line.separator"))
				.append("\t\t <url-pattern>/SecurityFilterManagement/*</url-pattern>").append(System.getProperty("line.separator"))
				.append("\t </servlet-mapping>").append(System.getProperty("line.separator"))
				.append("</web-app>").append(System.getProperty("line.separator"))
				.toString();			
		try {
			FileUtils.writeStringToFile(webxml,content,Charset.defaultCharset());
		} catch (IOException e) {
			logger.info(e.getMessage());		
		}
		return webxml;
	}


	/**
	 * Delete the project directory
	 * @param projectDir the name of the directory
	 */
	public static void deleteProjectFolder(String projectDir){

		try {
			FileUtils.deleteDirectory(new File(projectDir));
		} catch (IOException e) {
			logger.info(e.getMessage());
		}
	}

	/**
	 * Return the war file as an byte[]
	 * @param warFile The war file
	 * @return the byte[] of the war file
	 */
	public static byte[] getBytesFromWar(File warFile){
		byte[] bytes = null; 
		try {
			bytes = FileUtils.readFileToByteArray(warFile);
		} catch (IOException e) {
			logger.info(e.getMessage());
		}
		return bytes;
	}


	/**
	 * Create the log4j file
	 * @param projectDir the name of the log4j.properties file
	 * @param sfName
	 */
	public static void createLog4jProperties(String projectDir, String sfName){

		File log4j = new File(projectDir+System.getProperty("file.separator")+"WEB-INF"+System.getProperty("file.separator")+"classes"+System.getProperty("file.separator")+"log4j.properties");
		String content = new StringBuilder(propertiesLicenseHeader)
				.append("# Global logging configuration").append(System.getProperty("line.separator"))
				.append("log4j.rootLogger=INFO,daily").append(System.getProperty("line.separator"))
				.append("#log4j.rootLogger=info, stdout").append(System.getProperty("line.separator"))
				.append("# Console output").append(System.getProperty("line.separator"))
				.append("log4j.appender.stdout=org.apache.log4j.ConsoleAppender").append(System.getProperty("line.separator"))
				.append("log4j.appender.stdout.layout=org.apache.log4j.PatternLayout").append(System.getProperty("line.separator"))
				.append("log4j.appender.stdout.layout.ConversionPattern=%5p [%t] %c - %m%n \n").append(System.getProperty("line.separator"))
				.append("# daily Appender").append(System.getProperty("line.separator"))
				.append("log4j.appender.daily=org.apache.log4j.DailyRollingFileAppender").append(System.getProperty("line.separator"))
				.append("log4j.appender.daily.File=${catalina.home}${file.separator}logs${file.separator}"+sfName+".log").append(System.getProperty("line.separator"))
				.append("log4j.appender.daily.DatePattern='.'yyyy-MM-dd").append(System.getProperty("line.separator"))
				.append("log4j.appender.daily.layout=org.apache.log4j.PatternLayout").append(System.getProperty("line.separator"))
				.append("log4j.appender.daily.layout.ConversionPattern=%d %-5p [%t] %c - %m%n \n")
				.toString();
		try {
			FileUtils.writeStringToFile(log4j,content,Charset.defaultCharset());
		} catch (IOException e) {
			logger.info(e.getMessage());
		}
	}

	/**
	 * Create a java representation of the Security model
	 * @param projectDir the project directory  
	 * @param sfName the name of the security filter
	 * @param securityModel The security model
	 * @return
	 */
	public static File createSecurityModel(String projectDir, String sfName, byte[] securityModel){

		File coordFile = new File(projectDir+System.getProperty("file.separator")+"WEB-INF"+System.getProperty("file.separator")+"classes"+System.getProperty("file.separator")+sfName+".security");
		try {
			FileUtils.writeByteArrayToFile(coordFile, securityModel);
		} catch (IOException e) {
			throw new SFGeneratorException(e.getMessage());
		}
		return coordFile;
	}

	/**
	 * copy the war template file 
	 * @param destination th working directory
	 * @return The war file
	 * @throws IOException
	 */
	public static File copyWarTemplate( String destination) throws IOException{
		File dest  = new File(destination + File.separatorChar + WARTEMPLATENAME);
		final File jarFile = new File(Utilities.class.getProtectionDomain().getCodeSource().getLocation().getPath());

		if (jarFile.isFile()){
			final JarFile jar = new JarFile(Utilities.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			final Enumeration<JarEntry> entries =jar.entries();

			while(entries.hasMoreElements()){
				JarEntry entry = entries.nextElement();
				if(entry.getName().equals("war-template/" + WARTEMPLATENAME)){
					InputStream entrystream = jar.getInputStream(entry);
					FileUtils.copyInputStreamToFile(entrystream, dest);
				}

			}
		}else{
			File war = new File( "." + File.separatorChar + "src" + File.separatorChar + "main" + File.separatorChar + "resources" + File.separatorChar+"war-template" + File.separatorChar + WARTEMPLATENAME);

			FileUtils.copyFile(war, dest);
		}




		return dest;
	}

	/**
	 * add configuration file into the SF archive
	 * @param projectDir the working directory
	 * @param configurationFile The configuration file
	 */
	public static void addConfigFileintoWar(String projectDir, File configurationFile){
		Map<String, String> env = new HashMap<>();
		env.put("create", "true");
		Path path = Paths.get(projectDir +System.getProperty("file.separator") + WARTEMPLATENAME);
		URI uri = URI.create("jar:" + path.toUri());
		try (FileSystem fs = FileSystems.newFileSystem(uri, env))
		{
			Files.copy(configurationFile.toPath(), fs.getPath( "/WEB-INF/config.xml" ), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	}

	/**
	 * add jar file into the SF archive
	 * @param projectDir the working directory
	 * @param jarFile The jar file
	 */
	public static void addJarFileintoWar(String projectDir, byte[] jarFile){
		Map<String, String> env = new HashMap<>();
		env.put("create", "true");
		Path path = Paths.get(projectDir +System.getProperty("file.separator") + WARTEMPLATENAME);
		URI uri = URI.create("jar:" + path.toUri());
		try (FileSystem fs = FileSystems.newFileSystem(uri, env))
		{
			Files.write(fs.getPath( "/WEB-INF/lib/customSF.jar") , jarFile);
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	}


	/**
	 * add jar file into the SF archive
	 * @param projectDir the working directory
	 * @param path the path wer the file must be set
	 * @param file The jar file
	 */
	public static void addFileintoWar(String projectDir, String path, byte[] file){
		Map<String, String> env = new HashMap<>();
		env.put("create", "true");
		Path _path = Paths.get(projectDir +System.getProperty("file.separator") + WARTEMPLATENAME);
		URI uri = URI.create("jar:" + _path.toUri());
		try (FileSystem fs = FileSystems.newFileSystem(uri, env))
		{
			if (!Files.exists(fs.getPath( "/WEB-INF/" + path))){
				System.out.println("file " + path + " not exist" );
				Files.write(fs.getPath( "/WEB-INF/" + path) , file);
			}
			
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	}


	/**
	 * add jar file into the SF archive
	 * @param projectDir the working directory
	 * @param zipFile The zip file
	 * @throws IOException 
	 */
	public static void addZipFileintoWar(String projectDir, byte[] zipFile) throws IOException{

		ZipInputStream zipIn = new ZipInputStream(new ByteArrayInputStream(zipFile));
		ZipEntry ze;

		while ((ze = zipIn.getNextEntry()) != null) {

			if (!ze.isDirectory()) {
				//System.out.println("-" + ze.getName());
				byte[] data = unZipFromByteStream(zipFile, ze.getName());
				addFileintoWar(projectDir, ze.getName(), data);
			}			
			zipIn.closeEntry();
		}
		zipIn.close();
	}
	
	public static byte[] unZipFromByteStream(byte[] data, String fileName) {

	    ZipEntry entry = null;
	    ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(data));;
	    ByteArrayOutputStream out = null;
	    try {
	        while ((entry = zis.getNextEntry()) != null) {

	            if (!entry.isDirectory()) {
	                //System.out.println("-" + entry.getName());

	                out = new ByteArrayOutputStream();
	                byte[] byteBuff = new byte[1024];
	                int bytesRead = 0;
	                while ((bytesRead = zis.read(byteBuff)) != -1)
	                {
	                    out.write(byteBuff, 0, bytesRead);
	                }
	                out.close();
	                if(entry.getName().equals(fileName)){
	                	return (out == null) ? null : out.toByteArray();
	                }
	            }
	        }
	    } catch (Exception ex) {
	        ex.printStackTrace();
	    }
	    return null;
	}

	
	//
	//
	//
	//
	//
	//			ZipEntry entry = zipIn.getNextEntry();
	//			// iterates over entries in the zip file
	//			while (entry != null) {
	//				String filePath = "/tmp" + File.separator + entry.getName();
	//				System.out.println("entry name " + entry.getName() + " : " + entry.toString());
	//
	//				if (!entry.isDirectory()) {
	//					// if the entry is a file, extracts it
	//					//extractFile(zipIn, filePath);
	//
	//
	//					Map<String, String> env = new HashMap<>();
	//					env.put("create", "true");
	//					Path path = Paths.get(projectDir +System.getProperty("file.separator") + WARTEMPLATENAME);
	//					URI uri = URI.create("jar:" + path.toUri());
	//					try (FileSystem fs = FileSystems.newFileSystem(uri, env))
	//					{
	//						ByteArrayOutputStream out = new ByteArrayOutputStream();
	//						try (InputStream in = zipIn.getInputStream(entry)) {
	//							IOUtils.copy(in, out);
	//						}
	//						Files.write(fs.getPath( "/WEB-INF/"+ entry.getName()) , out.toByteArray());
	//					} catch (IOException e2) {
	//						// TODO Auto-generated catch block
	//						e2.printStackTrace();
	//					}
	//
	//
	//
	//				} else {
	//					// if the entry is a directory, make the directory
	//					File dir = new File(filePath);
	//					dir.mkdir();
	//				}
	//				zipIn.closeEntry();
	//				entry = zipIn.getNextEntry();
	//			}
	//			zipIn.close();


	//		}


	/**
	 * add web.xml file into the SF archive
	 * @param projectDir the working directory
	 * @param configurationFile The configuration file
	 */
	public static void addWebXmlFileintoWar(String projectDir, File configurationFile){
		Map<String, String> env = new HashMap<>();
		env.put("create", "true");
		Path path = Paths.get(projectDir +System.getProperty("file.separator") + WARTEMPLATENAME);
		URI uri = URI.create("jar:" + path.toUri());
		try (FileSystem fs = FileSystems.newFileSystem(uri, env))
		{
			Files.copy(configurationFile.toPath(), fs.getPath( "/WEB-INF/web.xml" ), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	}

	public static void addXACMLPoliciesFileintoWar(String projectDir, File xACMLPolicies) {
		Map<String, String> env = new HashMap<>();
		env.put("create", "true");
		Path path = Paths.get(projectDir +System.getProperty("file.separator") + WARTEMPLATENAME);
		URI uri = URI.create("jar:" + path.toUri());
		try (FileSystem fs = FileSystems.newFileSystem(uri, env))
		{
			Files.copy(xACMLPolicies.toPath(), fs.getPath( "/WEB-INF/policy.xml" ), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

	}
}
