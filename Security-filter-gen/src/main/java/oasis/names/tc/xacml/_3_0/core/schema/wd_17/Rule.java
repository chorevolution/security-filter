//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.08.05 at 10:36:14 PM CEST 
//


package oasis.names.tc.xacml._3_0.core.schema.wd_17;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for RuleType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RuleType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:tc:xacml:3.0:core:schema:wd-17}Description" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:tc:xacml:3.0:core:schema:wd-17}Target" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:tc:xacml:3.0:core:schema:wd-17}Condition" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:tc:xacml:3.0:core:schema:wd-17}ObligationExpressions" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:oasis:names:tc:xacml:3.0:core:schema:wd-17}AdviceExpressions" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="RuleId" use="required" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="Effect" use="required" type="{urn:oasis:names:tc:xacml:3.0:core:schema:wd-17}EffectType" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RuleType", propOrder = {
    "description",
    "target",
    "condition",
    "obligationExpressions",
    "adviceExpressions"
})
@XmlRootElement(name = "Rule")
public class Rule implements Serializable, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "Target")
    protected Target target;
    @XmlElement(name = "Condition")
    protected Condition condition;
    @XmlElement(name = "ObligationExpressions")
    protected ObligationExpressions obligationExpressions;
    @XmlElement(name = "AdviceExpressions")
    protected AdviceExpressions adviceExpressions;
    @XmlAttribute(name = "RuleId", required = true)
    protected String ruleId;
    @XmlAttribute(name = "Effect", required = true)
    protected EffectType effect;

    /**
     * Default no-arg constructor
     * 
     */
    protected Rule() {
        super();
    }

    /**
     * Fully-initialising value constructor
     * 
     */
    public Rule(final String description, final Target target, final Condition condition, final ObligationExpressions obligationExpressions, final AdviceExpressions adviceExpressions, final String ruleId, final EffectType effect) {
        this.description = description;
        this.target = target;
        this.condition = condition;
        this.obligationExpressions = obligationExpressions;
        this.adviceExpressions = adviceExpressions;
        this.ruleId = ruleId;
        this.effect = effect;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    protected void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the target property.
     * 
     * @return
     *     possible object is
     *     {@link Target }
     *     
     */
    public Target getTarget() {
        return target;
    }

    /**
     * Sets the value of the target property.
     * 
     * @param value
     *     allowed object is
     *     {@link Target }
     *     
     */
    protected void setTarget(Target value) {
        this.target = value;
    }

    /**
     * Gets the value of the condition property.
     * 
     * @return
     *     possible object is
     *     {@link Condition }
     *     
     */
    public Condition getCondition() {
        return condition;
    }

    /**
     * Sets the value of the condition property.
     * 
     * @param value
     *     allowed object is
     *     {@link Condition }
     *     
     */
    protected void setCondition(Condition value) {
        this.condition = value;
    }

    /**
     * Gets the value of the obligationExpressions property.
     * 
     * @return
     *     possible object is
     *     {@link ObligationExpressions }
     *     
     */
    public ObligationExpressions getObligationExpressions() {
        return obligationExpressions;
    }

    /**
     * Sets the value of the obligationExpressions property.
     * 
     * @param value
     *     allowed object is
     *     {@link ObligationExpressions }
     *     
     */
    protected void setObligationExpressions(ObligationExpressions value) {
        this.obligationExpressions = value;
    }

    /**
     * Gets the value of the adviceExpressions property.
     * 
     * @return
     *     possible object is
     *     {@link AdviceExpressions }
     *     
     */
    public AdviceExpressions getAdviceExpressions() {
        return adviceExpressions;
    }

    /**
     * Sets the value of the adviceExpressions property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdviceExpressions }
     *     
     */
    protected void setAdviceExpressions(AdviceExpressions value) {
        this.adviceExpressions = value;
    }

    /**
     * Gets the value of the ruleId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRuleId() {
        return ruleId;
    }

    /**
     * Sets the value of the ruleId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    protected void setRuleId(String value) {
        this.ruleId = value;
    }

    /**
     * Gets the value of the effect property.
     * 
     * @return
     *     possible object is
     *     {@link EffectType }
     *     
     */
    public EffectType getEffect() {
        return effect;
    }

    /**
     * Sets the value of the effect property.
     * 
     * @param value
     *     allowed object is
     *     {@link EffectType }
     *     
     */
    protected void setEffect(EffectType value) {
        this.effect = value;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Rule)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Rule that = ((Rule) object);
        {
            String lhsDescription;
            lhsDescription = this.getDescription();
            String rhsDescription;
            rhsDescription = that.getDescription();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "description", lhsDescription), LocatorUtils.property(thatLocator, "description", rhsDescription), lhsDescription, rhsDescription)) {
                return false;
            }
        }
        {
            Target lhsTarget;
            lhsTarget = this.getTarget();
            Target rhsTarget;
            rhsTarget = that.getTarget();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "target", lhsTarget), LocatorUtils.property(thatLocator, "target", rhsTarget), lhsTarget, rhsTarget)) {
                return false;
            }
        }
        {
            Condition lhsCondition;
            lhsCondition = this.getCondition();
            Condition rhsCondition;
            rhsCondition = that.getCondition();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "condition", lhsCondition), LocatorUtils.property(thatLocator, "condition", rhsCondition), lhsCondition, rhsCondition)) {
                return false;
            }
        }
        {
            ObligationExpressions lhsObligationExpressions;
            lhsObligationExpressions = this.getObligationExpressions();
            ObligationExpressions rhsObligationExpressions;
            rhsObligationExpressions = that.getObligationExpressions();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "obligationExpressions", lhsObligationExpressions), LocatorUtils.property(thatLocator, "obligationExpressions", rhsObligationExpressions), lhsObligationExpressions, rhsObligationExpressions)) {
                return false;
            }
        }
        {
            AdviceExpressions lhsAdviceExpressions;
            lhsAdviceExpressions = this.getAdviceExpressions();
            AdviceExpressions rhsAdviceExpressions;
            rhsAdviceExpressions = that.getAdviceExpressions();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "adviceExpressions", lhsAdviceExpressions), LocatorUtils.property(thatLocator, "adviceExpressions", rhsAdviceExpressions), lhsAdviceExpressions, rhsAdviceExpressions)) {
                return false;
            }
        }
        {
            String lhsRuleId;
            lhsRuleId = this.getRuleId();
            String rhsRuleId;
            rhsRuleId = that.getRuleId();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "ruleId", lhsRuleId), LocatorUtils.property(thatLocator, "ruleId", rhsRuleId), lhsRuleId, rhsRuleId)) {
                return false;
            }
        }
        {
            EffectType lhsEffect;
            lhsEffect = this.getEffect();
            EffectType rhsEffect;
            rhsEffect = that.getEffect();
            if (!strategy.equals(LocatorUtils.property(thisLocator, "effect", lhsEffect), LocatorUtils.property(thatLocator, "effect", rhsEffect), lhsEffect, rhsEffect)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            String theDescription;
            theDescription = this.getDescription();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "description", theDescription), currentHashCode, theDescription);
        }
        {
            Target theTarget;
            theTarget = this.getTarget();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "target", theTarget), currentHashCode, theTarget);
        }
        {
            Condition theCondition;
            theCondition = this.getCondition();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "condition", theCondition), currentHashCode, theCondition);
        }
        {
            ObligationExpressions theObligationExpressions;
            theObligationExpressions = this.getObligationExpressions();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "obligationExpressions", theObligationExpressions), currentHashCode, theObligationExpressions);
        }
        {
            AdviceExpressions theAdviceExpressions;
            theAdviceExpressions = this.getAdviceExpressions();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "adviceExpressions", theAdviceExpressions), currentHashCode, theAdviceExpressions);
        }
        {
            String theRuleId;
            theRuleId = this.getRuleId();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "ruleId", theRuleId), currentHashCode, theRuleId);
        }
        {
            EffectType theEffect;
            theEffect = this.getEffect();
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "effect", theEffect), currentHashCode, theEffect);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            String theDescription;
            theDescription = this.getDescription();
            strategy.appendField(locator, this, "description", buffer, theDescription);
        }
        {
            Target theTarget;
            theTarget = this.getTarget();
            strategy.appendField(locator, this, "target", buffer, theTarget);
        }
        {
            Condition theCondition;
            theCondition = this.getCondition();
            strategy.appendField(locator, this, "condition", buffer, theCondition);
        }
        {
            ObligationExpressions theObligationExpressions;
            theObligationExpressions = this.getObligationExpressions();
            strategy.appendField(locator, this, "obligationExpressions", buffer, theObligationExpressions);
        }
        {
            AdviceExpressions theAdviceExpressions;
            theAdviceExpressions = this.getAdviceExpressions();
            strategy.appendField(locator, this, "adviceExpressions", buffer, theAdviceExpressions);
        }
        {
            String theRuleId;
            theRuleId = this.getRuleId();
            strategy.appendField(locator, this, "ruleId", buffer, theRuleId);
        }
        {
            EffectType theEffect;
            theEffect = this.getEffect();
            strategy.appendField(locator, this, "effect", buffer, theEffect);
        }
        return buffer;
    }

}
