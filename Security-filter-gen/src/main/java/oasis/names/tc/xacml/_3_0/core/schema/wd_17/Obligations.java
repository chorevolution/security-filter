//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.08.05 at 10:36:14 PM CEST 
//


package oasis.names.tc.xacml._3_0.core.schema.wd_17;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.jvnet.jaxb2_commons.lang.Equals;
import org.jvnet.jaxb2_commons.lang.EqualsStrategy;
import org.jvnet.jaxb2_commons.lang.HashCode;
import org.jvnet.jaxb2_commons.lang.HashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBEqualsStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBHashCodeStrategy;
import org.jvnet.jaxb2_commons.lang.JAXBToStringStrategy;
import org.jvnet.jaxb2_commons.lang.ToString;
import org.jvnet.jaxb2_commons.lang.ToStringStrategy;
import org.jvnet.jaxb2_commons.locator.ObjectLocator;
import org.jvnet.jaxb2_commons.locator.util.LocatorUtils;


/**
 * <p>Java class for ObligationsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ObligationsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:oasis:names:tc:xacml:3.0:core:schema:wd-17}Obligation" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObligationsType", propOrder = {
    "obligations"
})
@XmlRootElement(name = "Obligations")
public class Obligations implements Serializable, Equals, HashCode, ToString
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "Obligation", required = true)
    protected List<Obligation> obligations;
    protected transient List<Obligation> obligations_RO = null;

    /**
     * Default no-arg constructor
     * 
     */
    protected Obligations() {
        super();
    }

    /**
     * Fully-initialising value constructor
     * 
     */
    public Obligations(final List<Obligation> obligations) {
        this.obligations = obligations;
    }

    public boolean equals(ObjectLocator thisLocator, ObjectLocator thatLocator, Object object, EqualsStrategy strategy) {
        if (!(object instanceof Obligations)) {
            return false;
        }
        if (this == object) {
            return true;
        }
        final Obligations that = ((Obligations) object);
        {
            List<Obligation> lhsObligations;
            lhsObligations = (((this.obligations!= null)&&(!this.obligations.isEmpty()))?this.getObligations():null);
            List<Obligation> rhsObligations;
            rhsObligations = (((that.obligations!= null)&&(!that.obligations.isEmpty()))?that.getObligations():null);
            if (!strategy.equals(LocatorUtils.property(thisLocator, "obligations", lhsObligations), LocatorUtils.property(thatLocator, "obligations", rhsObligations), lhsObligations, rhsObligations)) {
                return false;
            }
        }
        return true;
    }

    public boolean equals(Object object) {
        final EqualsStrategy strategy = JAXBEqualsStrategy.INSTANCE;
        return equals(null, null, object, strategy);
    }

    public int hashCode(ObjectLocator locator, HashCodeStrategy strategy) {
        int currentHashCode = 1;
        {
            List<Obligation> theObligations;
            theObligations = (((this.obligations!= null)&&(!this.obligations.isEmpty()))?this.getObligations():null);
            currentHashCode = strategy.hashCode(LocatorUtils.property(locator, "obligations", theObligations), currentHashCode, theObligations);
        }
        return currentHashCode;
    }

    public int hashCode() {
        final HashCodeStrategy strategy = JAXBHashCodeStrategy.INSTANCE;
        return this.hashCode(null, strategy);
    }

    public String toString() {
        final ToStringStrategy strategy = JAXBToStringStrategy.INSTANCE;
        final StringBuilder buffer = new StringBuilder();
        append(null, buffer, strategy);
        return buffer.toString();
    }

    public StringBuilder append(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        strategy.appendStart(locator, this, buffer);
        appendFields(locator, buffer, strategy);
        strategy.appendEnd(locator, this, buffer);
        return buffer;
    }

    public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
        {
            List<Obligation> theObligations;
            theObligations = (((this.obligations!= null)&&(!this.obligations.isEmpty()))?this.getObligations():null);
            strategy.appendField(locator, this, "obligations", buffer, theObligations);
        }
        return buffer;
    }

    public List<Obligation> getObligations() {
        if (this.obligations == null) {
            this.obligations = new ArrayList<Obligation>();
        }
        if (this.obligations_RO == null) {
            this.obligations_RO = ((obligations == null)?null:Collections.unmodifiableList(this.obligations));
        }
        return this.obligations_RO;
    }

}
