/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securityfilter.example;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletInputStream;
import eu.chorevolution.securityfilter.api.AdaptationAuthNInterface;
import eu.chorevolution.securityfilter.api.SFServletRequestWrapper;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.commons.io.IOUtils;
import org.w3c.dom.NodeList;
/**
 * Authentication ope.ation to validate the authentication realized to protect the choreography
 */
public class adaptationAuthNExample implements AdaptationAuthNInterface{

	@Override
	public SFServletRequestWrapper checkAdaptationAuthN(SFServletRequestWrapper request) {
		// TODO Auto-generated method stub
		System.out.println(" adaptationAuthNExample->checkAdaptationAuthN()");
		request.addHeader("test", "test");
		String pwd = "pwd";
		String token = SecurityHelper.getDynamicPassword(pwd); 
		request.addParameter("s", token);

		try {
			ServletInputStream stream = request.getInputStream();

//			String body = IOUtils.toString(request.getReader());
//
//			System.out.println("body :"+ body);


			MessageFactory messageFactory = MessageFactory.newInstance();
			SOAPMessage soapMessage = messageFactory.createMessage(new MimeHeaders(), stream);

			SOAPEnvelope env = soapMessage.getSOAPPart().getEnvelope();
			env.getBody()
			.addChildElement( env.createName( "Response" ) )
			.addTextNode( "This is a response" );
			NodeList credentials =  env.getBody().getElementsByTagName("name");
	
			int len = credentials.getLength();
			System.out.println("len" + len);
			for(int i = 0; i < len; i++) { 
			    credentials.item(i).setTextContent("new credential content goes here...");
			}
			
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			soapMessage.writeTo(out);
			//request.setContentLength(out.size());
			request.resetInputStream(out.toByteArray());

		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return request;
	}
}
